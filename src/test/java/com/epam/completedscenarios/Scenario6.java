package com.epam.completedscenarios;

import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.businessobjects.TrainingResultsPopUpBO;
import com.epam.businessobjects.UserAccountBO;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import io.qameta.allure.Description;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario6 extends BaseTest {
    private SportBuddiesMainBO sportBuddiesMainBO;
    private UserAccountBO userAccountBO;
    private TrainingResultsPopUpBO trainingResultsPopUpBO;

    @BeforeClass
    void init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        trainingResultsPopUpBO = new TrainingResultsPopUpBO();
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
    }

    @Description("As a user I want to see page with 3 tabs - PROFILE, ACHIEVEMENTS and REQUESTS.\n" +
            "Each tab should show appropriate user-related information.")
    @Test
    public void Scenario6Test() {
        trainingResultsPopUpBO.clickCancelButton();
        userAccountBO = sportBuddiesMainBO.goToUserAccountPage();
        CustomSoftAssert softAssert = new CustomSoftAssert();
        softAssert.assertTrue(userAccountBO.checkIfProfileButtonIsPresent(),"Profile button is not present.");
        softAssert.assertTrue(userAccountBO.checkIfAchievementsButtonIsPresent(),"Achievements button is not present.");
        softAssert.assertTrue(userAccountBO.checkIfRequestsButtonIsPresent(),"Requests button is not present.");
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}