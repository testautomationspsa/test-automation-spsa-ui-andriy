package com.epam.completedscenarios;

import com.epam.businessobjects.BlogBO;
import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import io.qameta.allure.Description;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario3 extends BaseTest {
    private static final String TEST_TAG = "testTag1";
    private SportBuddiesMainBO sportBuddiesMainBO;
    private BlogBO blogsBO;

    @BeforeClass
    void init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
    }

    @Description("As a User I want to see community blog with posts.\n" +
            "On UI blogs should looks like a list of cards.\n" +
            "SEE MORE button should navigate user to Blog details page\n" + // 1
            "As a user I want to search topics by tags\n" + // 2
            "Pagination at the bottom\n" + // 3
            "There should be ability for User to see blogpost details page by following link from blogs page.") // 1
    @Test
    public void Scenario3Test() {
        blogsBO = sportBuddiesMainBO.goToBlogPage();
        CustomSoftAssert softAssert = new CustomSoftAssert();
        softAssert.assertTrue(blogsBO.isPaginationPresent(),"Pagination is missing.");
        softAssert.assertTrue(sportBuddiesMainBO.checkForLogoPresence());
        blogsBO.clickOnFirstBlogReadMore() // 1
                .clickOnFirstTag() // 2
                .clickOnFirstBlogReadMore()
                .clickGoBackButton();
        softAssert.assertAll();
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
