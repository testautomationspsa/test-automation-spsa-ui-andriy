package com.epam.completedscenarios;

import com.epam.businessobjects.FindVenueBO;
import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.businessobjects.TrainingResultsPopUpBO;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import io.qameta.allure.Description;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario10 extends BaseTest {
    private SportBuddiesMainBO sportBuddiesMainBO;
    private FindVenueBO findVenueBO;
    private TrainingResultsPopUpBO trainingResultsPopUpBO;

    @BeforeClass
    void init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        trainingResultsPopUpBO = new TrainingResultsPopUpBO();
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
        trainingResultsPopUpBO.clickCancelButton();
        findVenueBO = sportBuddiesMainBO.clickSearchButon()
                .clickSearchLocation();
    }

    @Description("As a User I want to perform a search for venues to see where I can do my training. \n" +
            "Venues search criterias on the left \n" +
            "Want to find place for trainig?\n" +
            "Search criterias:\n" +
            "- Sports type (running, swimming, football, yoga ...)\n" +
            "- Location (near my home, other) - when clicking other then input field appear to search a location (google maps) - search is made within 20 km\n" +
            "- Venue type (indoor, outdoor, any).")
    @Test
    public void Scenario10Test() {
        CustomSoftAssert softAssert = new CustomSoftAssert();
        softAssert.assertTrue(findVenueBO.isSearchButtonPresent(), "Search Button is not Present.");
        findVenueBO.clickVenueTypeButton();
        softAssert.assertTrue(findVenueBO.isIndoorEnabled(), "Indoor Option is not enabled.");
        softAssert.assertTrue(findVenueBO.isOutdoorEnabled(), "Outdoor Option is not enabled.");
        findVenueBO.selectIndoorOption().clickLocationButton();
        softAssert.assertTrue(findVenueBO.isNearMyHomeEnabled(), "Near My Home Option is not enabled.");
        softAssert.assertTrue(findVenueBO.isOtherEnabled(), "Other Option is not enabled.");
        findVenueBO.selectOtherLocationOption().clickSportTypeButton();
        softAssert.assertTrue(findVenueBO.isLocationInputFieldEnabled(), "Loction input field is not enabled.");
        softAssert.assertTrue(findVenueBO.isRunningEnabled(), "Running Option is not enabled.");
        softAssert.assertTrue(findVenueBO.isSwimmingEnabled(), "Swimming Option is not enabled.");
        softAssert.assertTrue(findVenueBO.isFootballEnabled(), "Football Option is not enabled.");
        softAssert.assertTrue(findVenueBO.isYogaEnabled(), "Yoga Option is not enabled.");
        softAssert.assertAll();
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
