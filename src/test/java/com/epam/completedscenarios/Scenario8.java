package com.epam.completedscenarios;

import com.epam.businessobjects.*;
import com.epam.fortest.BaseTest;
import com.epam.models.TrainingRequestModel;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import com.epam.utils.Waiter;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import com.epam.utils.enums.TrainingTime;
import io.qameta.allure.Description;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;

public class Scenario8 extends BaseTest {
    private SportBuddiesMainBO sportBuddiesMainBO;
    private AddTrainingRequestPopUpBO addTrainingRequestPopUpBO;
    private AccountRequestsSectionBO accountRequestsSectionBO;
    private UserAccountBO userAccountBO;
    private TrainingResultsPopUpBO trainingResultsPopUpBO;

    @BeforeClass
    void init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        addTrainingRequestPopUpBO = new AddTrainingRequestPopUpBO();
        accountRequestsSectionBO = new AccountRequestsSectionBO();
        trainingResultsPopUpBO = new TrainingResultsPopUpBO();
        userAccountBO = new UserAccountBO();
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
        trainingResultsPopUpBO.clickCancelButton();
        userAccountBO.goToUserRequests();
    }

    @Description("As a User I want to create a requests to be searchable by other System Users.\n" +
            "All requests from users that match search params will appear on search page.\n" +
            "User can create unlimited amount of requests\n" +
            "To create a request, user should click ADD REQUEST button then modal with form will appear to fill necessary data for request (see search a partner for data) and then click CREATE button\n" +
            "To delete a request user should click DELETE button then modal will appear to confirm deletion.\n" +
            "To edit a request user should click a EDIT button then modal with edit form will appear to edit request data.")
    @Test
    public void Scenario8Test() {
        CustomSoftAssert softAssert = new CustomSoftAssert();
        Assert.assertFalse(accountRequestsSectionBO.areAnyRequestsPresent(),
                "There are training requests present, test is aborted!");
        verifyCorrectEditing(softAssert);
        verifySportTypeSetting(softAssert);
        verifyGenderSetting(softAssert);
        verifyMaturityLevelSetting(softAssert);
        verifyTrainingTimeSetting(softAssert);
        softAssert.assertAll();
    }

    private void verifyCorrectEditing(CustomSoftAssert softAssert) {
        TrainingRequestModel testModel = TrainingRequestModel.BASIC_TRAINING_REQUEST_MODEL;
        TrainingRequestModel editedTestModel = TrainingRequestModel.BASIC_TRAINING_REQUEST_MODEL
                .toBuilder().trainingTime(TrainingTime.ALL_DAY).build();
        accountRequestsSectionBO.clickCreateNewRequestButton()
                .fillTrainingRequest(testModel);
        addTrainingRequestPopUpBO.clickSubmitButton();
        Waiter.waitFor(Waiter.MICRO_WAIT);
        TrainingRequestModel responseModel = accountRequestsSectionBO.getFirstTrainingRequestModel();
        softAssert.assertEquals(testModel, responseModel, "Incorrect Training Request was created!");
        accountRequestsSectionBO.clickFirstEditRequestButton()
                .fillTrainingRequest(editedTestModel);
        accountRequestsSectionBO.clickSaveEditedRequestButton();
        Waiter.waitFor(Waiter.MICRO_WAIT);
        TrainingRequestModel editedResponseModel = accountRequestsSectionBO.getFirstTrainingRequestModel();
        softAssert.assertEquals(editedTestModel, editedResponseModel,
                "Training Request was edited incorrectly!");
        accountRequestsSectionBO.deleteFirstTrainingRequest().clickDeleteButton();
        Waiter.waitFor(Waiter.MICRO_WAIT);
        softAssert.assertFalse(accountRequestsSectionBO.areAnyRequestsPresent(),
                "Training request was not deleted!");
    }

    private void verifyTrainingTimeSetting(CustomSoftAssert softAssert) {
        Arrays.stream(TrainingTime.values()).forEach(trainingTime -> {
            TrainingRequestModel testModel = TrainingRequestModel.BASIC_TRAINING_REQUEST_MODEL
                    .toBuilder().trainingTime(trainingTime).build();
            accountRequestsSectionBO.clickCreateNewRequestButton()
                    .fillTrainingRequest(testModel)
                    .clickSubmitButton();
            Waiter.waitFor(Waiter.MICRO_WAIT);
            TrainingRequestModel responseModel = accountRequestsSectionBO.getFirstTrainingRequestModel();
            softAssert.assertEquals(testModel, responseModel, "Incorrect Training Request was created!");
            accountRequestsSectionBO.deleteFirstTrainingRequest().clickDeleteButton();
            Waiter.waitFor(Waiter.MICRO_WAIT);
            softAssert.assertFalse(accountRequestsSectionBO.areAnyRequestsPresent(),
                    "Training request was not deleted!");
        });
    }

    private void verifyMaturityLevelSetting(CustomSoftAssert softAssert) {
        Arrays.stream(MaturityLevel.values()).forEach(maturityLevel -> {
            TrainingRequestModel testModel = TrainingRequestModel.BASIC_TRAINING_REQUEST_MODEL
                    .toBuilder().maturityLevel(maturityLevel).build();
            accountRequestsSectionBO.clickCreateNewRequestButton()
                    .fillTrainingRequest(testModel)
                    .clickSubmitButton();
            Waiter.waitFor(Waiter.MICRO_WAIT);
            TrainingRequestModel responseModel = accountRequestsSectionBO.getFirstTrainingRequestModel();
            softAssert.assertEquals(testModel, responseModel, "Incorrect Training Request was created!");
            accountRequestsSectionBO.deleteFirstTrainingRequest().clickDeleteButton();
            Waiter.waitFor(Waiter.MICRO_WAIT);
            softAssert.assertFalse(accountRequestsSectionBO.areAnyRequestsPresent(),
                    "Training request was not deleted!");
        });
    }

    private void verifyGenderSetting(CustomSoftAssert softAssert) {
        Arrays.stream(Gender.values()).forEach(gender -> {
            TrainingRequestModel testModel = TrainingRequestModel.BASIC_TRAINING_REQUEST_MODEL
                    .toBuilder().gender(gender).build();
            accountRequestsSectionBO.clickCreateNewRequestButton()
                    .fillTrainingRequest(testModel)
                    .clickSubmitButton();
            Waiter.waitFor(Waiter.MICRO_WAIT);
            TrainingRequestModel responseModel = accountRequestsSectionBO.getFirstTrainingRequestModel();
            softAssert.assertEquals(testModel, responseModel, "Incorrect Training Request was created!");
            accountRequestsSectionBO.deleteFirstTrainingRequest().clickDeleteButton();
            Waiter.waitFor(Waiter.MICRO_WAIT);
            softAssert.assertFalse(accountRequestsSectionBO.areAnyRequestsPresent(),
                    "Training request was not deleted!");
        });
    }

    private void verifySportTypeSetting(CustomSoftAssert softAssert) {
        Arrays.stream(SportType.values()).forEach(sportType -> {
            TrainingRequestModel testModel = TrainingRequestModel.BASIC_TRAINING_REQUEST_MODEL
                    .toBuilder().sportType(sportType).build();
            accountRequestsSectionBO.clickCreateNewRequestButton()
                    .fillTrainingRequest(testModel)
                    .clickSubmitButton();
            Waiter.waitFor(Waiter.MICRO_WAIT);
            TrainingRequestModel responseModel = accountRequestsSectionBO.getFirstTrainingRequestModel();
            softAssert.assertEquals(testModel, responseModel, "Incorrect Training Request was created!");
            accountRequestsSectionBO.deleteFirstTrainingRequest().clickDeleteButton();
            Waiter.waitFor(Waiter.MICRO_WAIT);
            softAssert.assertFalse(accountRequestsSectionBO.areAnyRequestsPresent(),
                    "Training request was not deleted!");
        });
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
