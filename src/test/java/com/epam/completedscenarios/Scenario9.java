package com.epam.completedscenarios;

import com.epam.businessobjects.FindPartnersBO;
import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.businessobjects.TrainingResultsPopUpBO;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import com.epam.utils.enums.TrainingTime;
import io.qameta.allure.Description;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario9 extends BaseTest {
    private FindPartnersBO findPartnersBO;
    private SportBuddiesMainBO sportBuddiesMainBO;
    private TrainingResultsPopUpBO trainingResultsPopUpBO;

    @BeforeClass
    public void Scenario9init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        findPartnersBO = new FindPartnersBO();
        trainingResultsPopUpBO = new TrainingResultsPopUpBO();
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
        trainingResultsPopUpBO.clickCancelButton();
    }

    @Description("As a User I want to perform search to see my potential partners. Search criteria:\n" +
            "- Look for a partner in max 20 km radius from profile location\n" +
            "- Sport type (running, swimming, football, yoga) - 4 types for now\n" + // 1
            "- Running distance (only for running) - user enters min and max distance he can run\n" +
            "- Maturity level (Beginner, Middle, Pro) - enum\n" + // 2
            "- Training time (morning(5AM-12PM), noon(13PM-15PM), evening(16PM-12AM), ALL DAY) - dropdown\n" + // 3
            "- Gender (male, female, any) - dropdown/select\n" + // 4
            "Search should happen when user click SEARCH button. " +
            "NB User can fill in one or more criteria and look for a partner.\n" +
            "User should be able to email other user.") // 5
    @Test
    public void Scenario9Test() {
        sportBuddiesMainBO.clickSearchButon().clickSearchPartner();
        CustomSoftAssert softAssert = new CustomSoftAssert();
        softAssert.assertTrue(findPartnersBO.isEmailToOtherUserButtonEnabled(),
                "EmailToUser button disabled!"); // 5
        softAssert.assertTrue(findPartnersBO.checkSearchByGender(Gender.FEMALE),
                "Gender mismatch!"); // 4
        softAssert.assertTrue(findPartnersBO.checkSearchBySportType(SportType.SWIMMING),
                "Sport Type mismatch!"); // 1
        softAssert.assertTrue(findPartnersBO.checkSearchByMaturityLevel(MaturityLevel.BEGINNER),
                "Maturity level mismatch!"); // 2
        softAssert.assertTrue(findPartnersBO.checkSearchByTime(TrainingTime.MORNING),
                "Training Time mismatch!"); // 3
        softAssert.assertAll();
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
