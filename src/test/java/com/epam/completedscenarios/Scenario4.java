package com.epam.completedscenarios;

import com.epam.businessobjects.BlogDetailsBO;
import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import com.epam.utils.Waiter;
import io.qameta.allure.Description;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Scenario4 extends BaseTest {
    private static final String FIRST_COMMENT_TEXT = "COMMENT, FOR TESTING PURPOSES ONLY";
    private static final String EDITED_COMMENT_TEXT = "EDITED COMMENT, FOR TESTING PURPOSES ONLY";
    private SportBuddiesMainBO sportBuddiesMainBO;
    private BlogDetailsBO blogDetailsBO;

    @BeforeMethod
    public void Scenario9init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        blogDetailsBO = new BlogDetailsBO();
    }

    @Description("As a logged-in User I want to have ability to comment below blogposts.\n" +
            "There should be text field with COMMENT button and list of previous comments underneath.\n" + // 1
            "When I comment the list should be synced with server \n" +
            "Comments should be visible to all users (logged-in and not logged-in),\n" + // 2
            "but ability to comment is just for logged-in users\n" + // 3
            "Logged in users can leave, edit and delete comments \n" + // 4
            "reply to other users comments\n" + // 5
            "like and dislike comments.") // 6
    @Test
    public void Scenario4SignedInTestUserTest() {
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
        CustomSoftAssert softAssert = new CustomSoftAssert();
        blogDetailsBO = sportBuddiesMainBO.goToBlogPage()
                .clickOnFirstBlogReadMore();
        blogDetailsBO.addComment(FIRST_COMMENT_TEXT);
        Waiter.waitFor(Waiter.MICRO_WAIT * 2);
        softAssert.assertEquals(blogDetailsBO.getFirstCommentTextComment(),
                FIRST_COMMENT_TEXT, "Comment was not correctly added.");
        blogDetailsBO.editComment(EDITED_COMMENT_TEXT);
        Waiter.waitFor(Waiter.MICRO_WAIT * 2);
        softAssert.assertEquals(blogDetailsBO.getFirstCommentTextComment(),
                EDITED_COMMENT_TEXT, "Comment was not correctly edited.");
        blogDetailsBO.deleteFirstComment();
        Waiter.waitFor(Waiter.MICRO_WAIT * 2);
        softAssert.assertNotEquals(blogDetailsBO.getFirstCommentTextComment(),
                EDITED_COMMENT_TEXT, "Comment was not successfully deleted.");
        softAssert.assertTrue(blogDetailsBO.isPostCommentButtonEnabled(),
                "Post comment button is not enabled for Signed In user.");
        softAssert.assertTrue(blogDetailsBO.areEditCommentsButtonsEnabled(),
                "Edit comment buttons are not enabled for Signed In user.");
        softAssert.assertTrue(blogDetailsBO.areReplyCommentButtonsEnabled(),
                "Reply comment buttons are not enabled for Signed In user.");
        softAssert.assertTrue(blogDetailsBO.areDeleteCommentButtonsEnabled(),
                "Delete comment buttons are not enabled for Signed In user.");
        softAssert.assertTrue(blogDetailsBO.areUpVoteCommentButtonsEnabled(),
                "Up Vote comment buttons are not enabled for Signed In user.");
        softAssert.assertTrue(blogDetailsBO.areDownVoteCommentButtonsEnabled(),
                "Down Vote comment buttons are not enabled for Signed In user.");
        softAssert.assertAll();
    }

    @Test
    public void Scenario4SignedOffTestUserTest() {
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .goToBlogPage()
                .clickOnFirstBlogReadMore();
        CustomSoftAssert softAssert = new CustomSoftAssert();
        softAssert.assertFalse(blogDetailsBO.isPostCommentButtonPresent(),
                "Post comment button is present for not Signed in user");
        softAssert.assertFalse(blogDetailsBO.areEditCommentsButtonsPresent(),
                "Edit comment buttons are present for not Signed in user");
        softAssert.assertFalse(blogDetailsBO.areDeleteCommentButtonsPresent(),
                "Delete comment buttons are present for not Signed in user");
        softAssert.assertFalse(blogDetailsBO.areReplyCommentButtonsPresent(),
                "Reply to comment buttons are present for not Signed in user");
        softAssert.assertFalse(blogDetailsBO.areDownVoteCommentButtonsEnabled(),
                "Down Vote comment buttons are enabled for not Signed in user");
        softAssert.assertFalse(blogDetailsBO.areUpVoteCommentButtonsEnabled(),
                "Up Vote comment buttons are enabled for not Signed in user");
        softAssert.assertAll();
    }

    @AfterTest(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
