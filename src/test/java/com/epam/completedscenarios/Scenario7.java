package com.epam.completedscenarios;

import com.epam.businessobjects.AccountProfileSectionBO;
import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.businessobjects.TrainingResultsPopUpBO;
import com.epam.fortest.BaseTest;
import com.epam.models.ProfileModel;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import io.qameta.allure.Description;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario7 extends BaseTest {
    private SportBuddiesMainBO sportBuddiesMainBO;
    private AccountProfileSectionBO accountProfileSectionBO;
    private TrainingResultsPopUpBO trainingResultsPopUpBO;

    @BeforeClass
    void init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        trainingResultsPopUpBO = new TrainingResultsPopUpBO();
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
    }

    @Description("As a user I want to see my profile data. This should be first tab of tabs list.\n" +
            "User should be able to edit and save his profile information by clicking SAVE button below user data.\n" +
            "User information visible on this tab:\n" +
            "- first name\n" +
            "- last name\n" +
            "- sport type (swimming, running, yoga, football) - cancelled\n" +
            "- maturity (beginner, middle, pro) - cancelled\n" +
            "- location\n" +
            "- gender(male, female)\n" +
            "- age (choose date of birth)\n" +
            "- photo (save pic, delete pic)\n" +
            "Upload photo (TBD)\n" +
            "Delete account - an email will be sent(TESTING MANUALLY).")
    @Test
    public void Scenario7Test() {
        trainingResultsPopUpBO.clickCancelButton();
        accountProfileSectionBO = sportBuddiesMainBO.goToUserAccountPage()
                .goToUserProfile()
                .fillProfileInfo(ProfileModel.CORRECT_TEST_PROFILE_MODEL)
                .clickSaveButton();
        CustomSoftAssert softAssert = new CustomSoftAssert();
        softAssert.assertEquals(
                ProfileModel.CORRECT_TEST_PROFILE_MODEL.getEmail(),accountProfileSectionBO.getEmail(),"wrong Email.");
        softAssert.assertEquals(
                ProfileModel.CORRECT_TEST_PROFILE_MODEL.getFirstName(),accountProfileSectionBO.getFirstName(),"wrong first name.");
        softAssert.assertEquals(
                ProfileModel.CORRECT_TEST_PROFILE_MODEL.getSecondName(),accountProfileSectionBO.getLastName(),"wrong last name.");
        softAssert.assertEquals(
                ProfileModel.CORRECT_TEST_PROFILE_MODEL.getAddress(),accountProfileSectionBO.getLocation(),"wrong address.");
        softAssert.assertEquals(
                ProfileModel.CORRECT_TEST_PROFILE_MODEL.getGender(),accountProfileSectionBO.getGender(),"wrong Gender.");
        softAssert.assertEquals(
                ProfileModel.CORRECT_TEST_PROFILE_MODEL.getDateOfBirthday(),accountProfileSectionBO.getBirthDate(),"wrong Birth date.");
        softAssert.assertAll();
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
