package com.epam.completedscenarios;

import com.epam.businessobjects.LoginBO;
import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.businessobjects.UserAccountBO;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import io.qameta.allure.Description;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario5 extends BaseTest {
    private SportBuddiesMainBO sportBuddiesBO;
    private LoginBO loginBO;
    private UserAccountBO userAccountBO;

    @BeforeClass
    void init() {
        sportBuddiesBO = new SportBuddiesMainBO();
        loginBO = new LoginBO();
        userAccountBO = new UserAccountBO();
    }

    @Description("As a user I want to sign in with my credentials.\n" +
            "System should use email and password pair to sign in user\n" +
            "After sign in user is redirected to profile page and suggestion to fill rest of info should appear.\n" +
            "Validation\n" +
            "Email: ^([A-z0-9_-]+\\\\.)*[A-z0-9_-]+@[a-z0-9_-]+(\\\\.[a-z0-9_-]+)*\\\\.[a-z]{2,6}$\n" +
            "Password: ^[a-zA-Z0-9]{8,25}.")
    @Test
    public void Scenario5Test() {
        CustomSoftAssert softAssert = new CustomSoftAssert();
        sportBuddiesBO.goToSportBuddiesMainPage()
                .clickSignInButton();
        loginBO.fillLoginAndPassword(UserModel.TEST_USER_INCORRECT)
                .submit();
        softAssert.assertTrue(loginBO.isErrorMessageDisplayed(),"Error message is not displayed.");
        loginBO.fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
        softAssert.assertTrue(userAccountBO.isOnUserProfilePage(),"Is not on user profile page.");
        softAssert.assertAll();
    }

    @AfterMethod(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
