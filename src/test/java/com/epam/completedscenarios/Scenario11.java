package com.epam.completedscenarios;

import com.epam.businessobjects.FindVenueBO;
import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.businessobjects.TrainingResultsPopUpBO;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.utils.DriverManager;
import com.epam.utils.Waiter;
import io.qameta.allure.Description;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario11 extends BaseTest {
    private SportBuddiesMainBO sportBuddiesMainBO;
    private FindVenueBO findVenueBO;
    private TrainingResultsPopUpBO trainingResultsPopUpBO;

    @BeforeClass
    void init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        trainingResultsPopUpBO = new TrainingResultsPopUpBO();
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
        trainingResultsPopUpBO.clickCancelButton();
        findVenueBO = sportBuddiesMainBO.clickSearchButon()
                .clickSearchLocation();
    }

    @Description("As a User I want to perform a search for venues to see where I can do my training. \n" +
            "After clicking on venue on the map,\n" +
            "I want to see Location Name and 'More info' button,\n" +
            "Which redirects me to Specific Venue Description Page.")
    @Test
    public void Scenario11Test() {
        findVenueBO.clickSportTypeButton()
                .selectRunningSportType()
                .clickLocationButton()
                .selectOtherLocationOption()
                .writeInVenueSearchField("Kyiv")
                .selectFirstSuggestion()
                .clickVenueTypeButton()
                .selectIndoorOption()
                .clickSearchButton()
                .clickOnTestVenueMarker()
                .clickMoreInfoButton();
        Waiter.waitFor(Waiter.SMALL_WAIT);
        Assert.assertTrue(findVenueBO.isVenueDetailsLabelDisplayed(), "Not on Venue Details section.");
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
