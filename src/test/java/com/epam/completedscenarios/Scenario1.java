package com.epam.completedscenarios;

import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.fortest.BaseTest;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import io.qameta.allure.Description;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario1 extends BaseTest {
    private SportBuddiesMainBO sportBuddiesMainBO;

    @BeforeClass
    void init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
    }

    @Description("As a User I want to see header which is always sticked to page top.\n" +
            "Not Logged-in User should see:\n" +
            "- logo on the left side\n" + // 1
            "- Blog + Sign-up/Sign-in links on the right side (already have an acc? SU/SI).") // 2
    @Test
    public void Scenario1Test() {
        CustomSoftAssert softAssert = new CustomSoftAssert();
        sportBuddiesMainBO.goToSportBuddiesMainPage();
        softAssert.assertTrue(sportBuddiesMainBO.checkForLogoPresence(), "Logo not present"); // 1
        softAssert.assertTrue(sportBuddiesMainBO.checkForSignInButtonPresence(), "Sign In button not present"); // 2
        softAssert.assertTrue(sportBuddiesMainBO.checkForSignUpButtonPresence(), "Sign Up button not present"); // 2
        softAssert.assertAll();
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}
