package com.epam.completedscenarios;

import com.epam.businessobjects.SportBuddiesMainBO;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.utils.CustomSoftAssert;
import com.epam.utils.DriverManager;
import io.qameta.allure.Description;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scenario2 extends BaseTest {
    private SportBuddiesMainBO sportBuddiesMainBO;

    @BeforeClass
    void init() {
        sportBuddiesMainBO = new SportBuddiesMainBO();
        sportBuddiesMainBO.goToSportBuddiesMainPage()
                .clickSignInButton()
                .fillLoginAndPassword(UserModel.TEST_USER)
                .submit();
    }

    @Description("Logged-in User should see:\n" +
            "- logo on the left side\n" +
            "- Blog + Search dropdown + User profile dropdown.")
    @Test
    public void Scenario2Test() {
        CustomSoftAssert softAssert = new CustomSoftAssert();
        softAssert.assertTrue(sportBuddiesMainBO.checkForLogoPresence(),"Logo not Present");
        softAssert.assertTrue(sportBuddiesMainBO.checkForBlogButtonPresence(),"Blog Button not present");
        softAssert.assertTrue(sportBuddiesMainBO.checkForSearchDropdownPresence(),"Search dropdown not present");
        softAssert.assertTrue(sportBuddiesMainBO.checkForUserProfileDropdownPresence(),"User account dropdown not present");
        softAssert.assertAll();
    }

    @AfterClass(alwaysRun = true)
    void quit() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }
}