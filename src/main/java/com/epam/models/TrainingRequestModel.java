package com.epam.models;

import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import com.epam.utils.enums.TrainingTime;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder(toBuilder = true)
public class TrainingRequestModel {
    public static TrainingRequestModel BASIC_TRAINING_REQUEST_MODEL =
            TrainingRequestModel.builder()
                    .trainingTime(TrainingTime.EVENING)
                    .gender(Gender.MALE)
                    .maturityLevel(MaturityLevel.BEGINNER)
                    .sportType(SportType.RUNNING)
                    .minRunningDistance(1)
                    .maxRunningDistance(50).build();
    private Gender gender;
    private MaturityLevel maturityLevel;
    private SportType sportType;
    private TrainingTime trainingTime;
    @EqualsAndHashCode.Exclude
    private Integer minRunningDistance;
    @EqualsAndHashCode.Exclude
    private Integer maxRunningDistance;
}
