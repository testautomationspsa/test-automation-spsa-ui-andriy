package com.epam.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Data
@Getter
@Builder(toBuilder = true)
public class ProfileModel {
    public static final ProfileModel CORRECT_TEST_PROFILE_MODEL =
            ProfileModel.builder()
                    .email(UserModel.TEST_USER.getEmail())
                    .firstName("FirstName")
                    .secondName("SecondName")
                    .dateOfBirthday("1/1/2001")
                    .gender("Male")
                    .address("Kyiv, Україна").build();
    private String address;
    private String dateOfBirthday;
    private String email;
    @EqualsAndHashCode.Exclude
    private String fileName;
    private String firstName;
    private String gender;
    private String secondName;
}
