package com.epam.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

@Data
@AllArgsConstructor
public class UserModel {
    public static final UserModel TEST_VENUE_ADMIN =
            new UserModel(getProperties().getProperty("TEST_VENUE_ADMIN"),
                    getProperties().getProperty("TEST_VENUE_ADMIN_PASSWORD"), "");
    public static final UserModel TEST_SUPER_ADMIN =
            new UserModel(getProperties().getProperty("TEST_SUPER_ADMIN"),
                    getProperties().getProperty("TEST_SUPER_ADMIN_PASSWORD"), "");
    public static final UserModel TEST_USER =
            new UserModel(getProperties().getProperty("TEST_USER"),
                    getProperties().getProperty("TEST_USER_PASSWORD"), "");
    public static final UserModel TEST_TRAINER =
            new UserModel(getProperties().getProperty("TEST_TRAINER"),
                    getProperties().getProperty("TEST_TRAINER_PASSWORD"), "");
    public static final UserModel TEST_USER_INCORRECT =
            new UserModel(getProperties().getProperty("TEST_USER"),
                    "badPassword", "");
    private String email;
    private String password;
    private String token;

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = UserModel.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            Logger.getAnonymousLogger().warning("NumberFormatException found.");
        } catch (IOException ex) {
            Logger.getAnonymousLogger().warning("IOException found.");
        }
        return prop;
    }
}
