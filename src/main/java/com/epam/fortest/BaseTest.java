package com.epam.fortest;

import com.epam.utils.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;

public class BaseTest {
    private static final Logger LOGGER = LogManager.getLogger(BaseTest.class);

    public WebDriver getDriver() {
        return DriverManager.getDriver();
    }

    public void onTestFailure(ITestResult iTestResult) {
        LOGGER.info("===TEST FAILED===");
    }

    protected void afterActions(ITestResult iTestResult) {
        if (!iTestResult.isSuccess()) {
            onTestFailure(iTestResult);
        }
    }
}
