package com.epam.businessobjects;

import com.epam.pageobjects.implementations.BlogsPage;

public class BlogBO {
    private BlogsPage blogsPage = new BlogsPage();

    public boolean isLogoPresent() {
        return blogsPage.isLogoPresent();
    }

    public boolean isPaginationPresent() {
        return blogsPage.isPaginationPresent();
    }

    public boolean isOnBlogPage() {
        return blogsPage.isOnBlogPage();
    }

    public BlogDetailsBO clickOnFirstBlogReadMore() {
        blogsPage.clickOnFirstBlogDetailsButton();
        return new BlogDetailsBO();
    }

    public BlogBO writeInSearchField(String text) {
        blogsPage.writeInSearchByTagField(text);
        return this;
    }

    public BlogBO clickSearchButton() {
        blogsPage.clickSearchByTagButton();
        return this;
    }
}
