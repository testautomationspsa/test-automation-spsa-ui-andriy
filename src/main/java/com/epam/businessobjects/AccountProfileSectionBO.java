package com.epam.businessobjects;

import com.epam.models.ProfileModel;
import com.epam.pageobjects.implementations.AccountProfileSectionPage;

public class AccountProfileSectionBO {
    private AccountProfileSectionPage accountProfileSectionPage = new AccountProfileSectionPage();
    private UserAccountBO userAccountBO = new UserAccountBO();

    public AccountProfileSectionBO goToProfileSection() {
        userAccountBO.goToUserProfile();
        return this;
    }

    public AccountProfileSectionBO fillProfileInfo(ProfileModel profileModel) {
        //accountProfileSectionPage.fillEmailField(profileModel.getEmail());
        accountProfileSectionPage.fillFirstNameField(profileModel.getFirstName());
        accountProfileSectionPage.fillLastNameField(profileModel.getSecondName());
        accountProfileSectionPage.chooseGenderField(profileModel.getGender());
        accountProfileSectionPage.fillLocationField(profileModel.getAddress());
        accountProfileSectionPage.fillBirthDateField(profileModel.getDateOfBirthday());
        accountProfileSectionPage.clickSaveButton();
        return this;
    }

    public String getEmail(){
        return accountProfileSectionPage.getEmail();
    }

    public String getFirstName(){
        return accountProfileSectionPage.getFirstName();
    }

    public String getLastName(){
        return accountProfileSectionPage.getLastName();
    }

    public String getGender(){
        return accountProfileSectionPage.getGender();
    }

    public String getLocation(){
        return accountProfileSectionPage.getLocation();
    }

    public String getBirthDate(){
        return accountProfileSectionPage.getBirthDate();
    }

    public AccountProfileSectionBO fillEmailField(String email) {
        accountProfileSectionPage.fillEmailField(email);
        return this;
    }

    public AccountProfileSectionBO fillFirstNameField(String firstName) {
        accountProfileSectionPage.fillFirstNameField(firstName);
        return this;
    }

    public AccountProfileSectionBO fillLastNameField(String lastName) {
        accountProfileSectionPage.fillLastNameField(lastName);
        return this;
    }

    public AccountProfileSectionBO fillLocationField(String location) {
        accountProfileSectionPage.fillLocationField(location);
        return this;
    }

    public AccountProfileSectionBO fillBirthDateField(String birthDate) {
        accountProfileSectionPage.fillBirthDateField(birthDate);
        return this;
    }

    public AccountProfileSectionBO clickSaveButton() {
        accountProfileSectionPage.clickSaveButton();
        return this;
    }
}
