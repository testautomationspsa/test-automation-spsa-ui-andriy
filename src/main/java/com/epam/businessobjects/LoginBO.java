package com.epam.businessobjects;

import com.epam.models.UserModel;
import com.epam.pageobjects.implementations.LoginPage;

public class LoginBO {
    private LoginPage loginPage = new LoginPage();

    public LoginBO goToLoginPage(){
        loginPage.goToSportBuddiesLoginPage();
        return this;
    }

    public LoginBO fillLoginAndPassword(UserModel user){
        loginPage.fillLoginField(user.getEmail());
        loginPage.fillPasswordField(user.getPassword());
        return this;
    }

    public LoginBO submit() {
        loginPage.submitLoginAndPassword();
        return this;
    }

    public boolean isLoginFieldDisplayed(){
        return loginPage.isLoginFieldDisplayed();
    }

    public boolean isErrorMessageDisplayed(){
        return loginPage.isErrorMessageDisplayed();
    }

    public boolean isOnLoginPage(){
        return loginPage.isOnLoginPage();
    }
}
