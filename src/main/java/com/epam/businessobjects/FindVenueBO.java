package com.epam.businessobjects;

import com.epam.pageobjects.implementations.FindVenuePage;

public class FindVenueBO {
    private FindVenuePage findVenuePage = new FindVenuePage();

    public FindVenueBO selectRunningSportType() {
        findVenuePage.clickSportTypeRUNNINGButton();
        return this;
    }

    public FindVenueBO selectIndoorOption() {
        findVenuePage.clickIndoorOption();
        return this;
    }

    public FindVenueBO selectOtherLocationOption() {
        findVenuePage.clickOtherOption();
        return this;
    }

    public FindVenueBO writeInVenueSearchField(String location) {
        findVenuePage.writeLocation(location);
        return this;
    }

    public FindVenueBO selectFirstSuggestion() {
        findVenuePage.selectFirstSuggestion();
        return this;
    }

    public FindVenueBO clickOnTestVenueMarker() {
        findVenuePage.clickTestVenueOnMap();
        return this;
    }

    public FindVenueBO clickMoreInfoButton() {
        findVenuePage.clickTestVenueMoreInfoButton();
        return this;
    }

    public FindVenueBO clickSearchButton() {
        findVenuePage.clickSearchButton();
        return this;
    }

    public FindVenueBO clickSportTypeButton() {
        findVenuePage.clickSportTypeButton();
        return this;
    }

    public boolean isRunningEnabled() {
        return findVenuePage.isRunningEnabled();
    }

    public boolean isSwimmingEnabled() {
        return findVenuePage.isSwimmingEnabled();
    }

    public boolean isFootballEnabled() {
        return findVenuePage.isFootballEnabled();
    }

    public boolean isYogaEnabled() {
        return findVenuePage.isYogaEnabled();
    }

    public FindVenueBO clickLocationButton() {
        findVenuePage.clickLocationButton();
        return this;
    }

    public boolean isNearMyHomeEnabled() {
        return findVenuePage.isNearMyHomeEnabled();
    }

    public boolean isOtherEnabled() {
        return findVenuePage.isOtherEnabled();
    }

    public FindVenueBO clickVenueTypeButton() {
        findVenuePage.clickVenueTypeButton();
        return this;
    }

    public boolean isIndoorEnabled() {
        return findVenuePage.isIndoorEnabled();
    }

    public boolean isOutdoorEnabled() {
        return findVenuePage.isOutdoorEnabled();
    }

    public boolean isSearchButtonPresent() {
        return findVenuePage.isSearchButtonPresent();
    }

    public boolean isMoreInfoButtonDisplayed() {
        return findVenuePage.isMoreInfoButtonDisplayed();
    }

    public boolean isVenueDetailsLabelDisplayed() {
        return findVenuePage.isVenueDetailsLabelDisplayed();
    }

    public boolean isLocationInputFieldEnabled() {
        return findVenuePage.isLocationInputFieldEnabled();
    }
}
