package com.epam.businessobjects;

import com.epam.models.TrainingRequestModel;
import com.epam.pageobjects.implementations.AddTrainingRequestPopUpPage;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import com.epam.utils.enums.TrainingTime;

public class AddTrainingRequestPopUpBO {
    private AddTrainingRequestPopUpPage addTrainingRequestPopUpPage = new AddTrainingRequestPopUpPage();

    public AddTrainingRequestPopUpBO fillTrainingRequest(TrainingRequestModel trainingRequestModel) {
        addTrainingRequestPopUpPage = new AddTrainingRequestPopUpPage();
        setSportType(trainingRequestModel.getSportType());
        if (trainingRequestModel.getSportType().equals(SportType.RUNNING)) {
            setRunningDistances(trainingRequestModel);
        }
        setMaturityLevel(trainingRequestModel.getMaturityLevel());
        setGender(trainingRequestModel.getGender());
        setTrainingTime(trainingRequestModel.getTrainingTime());
        return this;
    }

    public void clickSubmitButton() {
        addTrainingRequestPopUpPage = new AddTrainingRequestPopUpPage();
        addTrainingRequestPopUpPage.clickSubmitButton();
    }

    private void setSportType(SportType sportType) {
        addTrainingRequestPopUpPage.clickSportTypeButton();
        switch (sportType) {
            case RUNNING:
                addTrainingRequestPopUpPage.clickSportTypeRunningButton();
                break;
            case YOGA:
                addTrainingRequestPopUpPage.clickSportTypeYogaButton();
                break;
            case FOOTBALL:
                addTrainingRequestPopUpPage.clickSportTypeFootballButton();
                break;
            case SWIMMING:
                addTrainingRequestPopUpPage.clickSportTypeSwimmingButton();
                break;
        }
    }

    private void setRunningDistances(TrainingRequestModel trainingRequestModel) {
        addTrainingRequestPopUpPage.fillMinRunningDistance(trainingRequestModel.getMinRunningDistance());
        addTrainingRequestPopUpPage.fillMaxRunningDistance(trainingRequestModel.getMaxRunningDistance());
    }

    private void setGender(Gender gender) {
        switch (gender) {
            case MALE:
                addTrainingRequestPopUpPage.checkingOutMaleCheckBox();
                break;
            case FEMALE:
                addTrainingRequestPopUpPage.checkingOutFemaleCheckBox();
                break;
            case ANY:
                addTrainingRequestPopUpPage.checkingOutBothGendersCheckBox();
                break;
        }
    }

    private void setMaturityLevel(MaturityLevel maturityLevel) {
        switch (maturityLevel) {
            case BEGINNER:
                addTrainingRequestPopUpPage.checkingOutBeginnerCheckBox();
                break;
            case MIDDLE:
                addTrainingRequestPopUpPage.checkingOutMiddleCheckBox();
                break;
            case PRO:
                addTrainingRequestPopUpPage.checkingOutProfessionalCheckBox();
                break;
        }
    }

    private void setTrainingTime(TrainingTime trainingTime) {
        switch (trainingTime) {
            case MORNING:
                addTrainingRequestPopUpPage.checkingOutMorningCheckBox();
                break;
            case AFTERNOON:
                addTrainingRequestPopUpPage.checkingOutAfternoonCheckBox();
                break;
            case EVENING:
                addTrainingRequestPopUpPage.checkingOutEveningCheckBox();
                break;
            case ALL_DAY:
                addTrainingRequestPopUpPage.checkingOutAllDayCheckBox();
                break;
        }
    }
}
