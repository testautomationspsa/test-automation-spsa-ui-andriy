package com.epam.businessobjects;

import com.epam.pageobjects.implementations.BlogDetailsPage;
import com.epam.utils.Waiter;

public class BlogDetailsBO {
    private BlogDetailsPage blogDetailsPage = new BlogDetailsPage();

    public boolean isOnBlogPostDetailsPage() {
        return blogDetailsPage.isOnBlogDetailsPage();
    }

    public BlogDetailsBO addComment(String comment) {
        blogDetailsPage.writeComment(comment)
                .clickPostCommentButton();
        return this;
    }

    public BlogDetailsBO editComment(String comment) {
        blogDetailsPage.clickFirstEditCommentButton()
                .editComment(comment);
        Waiter.waitFor(Waiter.MICRO_WAIT);
        blogDetailsPage.clickPostEditedCommentButton();
        return this;
    }

    public BlogDetailsBO replyToComment(String comment) {
        blogDetailsPage.clickFirstReplyCommentButton()
                .writeComment(comment)
                .clickPostCommentButton();
        return this;
    }

    public BlogDetailsBO upVoteFirstComment() {
        blogDetailsPage.clickFirstUpVoteCommentButton();
        return this;
    }

    public BlogDetailsBO downVoteFirstComment() {
        blogDetailsPage.clickFirstDownVoteCommentButton();
        return this;
    }

    public Integer getUpVotesForFirstComment() {
        return blogDetailsPage.getFirstCommentUpVoteCounter();
    }

    public Integer getDownVotesForFirstComment() {
        return blogDetailsPage.getFirstCommentDownVoteCounter();
    }

    public BlogDetailsBO deleteFirstComment() {
        blogDetailsPage.clickFirstDeleteCommentButton();
        return this;
    }

    public String getFirstCommentTextComment() {
        return blogDetailsPage.getFirstCommentText();
    }

    public BlogBO clickOnFirstTag() {
        blogDetailsPage.clickOnFirstTag();
        return new BlogBO();
    }

    public BlogBO clickGoBackButton() {
        blogDetailsPage.clickGoBackButton();
        return new BlogBO();
    }

    public boolean isPostCommentButtonEnabled() {
        return blogDetailsPage.isPostCommentButtonEnabled();
    }

    public boolean isPostCommentButtonPresent() {
        return blogDetailsPage.isPostCommentButtonPresent();
    }

    public boolean areEditCommentsButtonsEnabled() {
        return blogDetailsPage.areEditCommentButtonsEnabled();
    }

    public boolean areEditCommentsButtonsPresent() {
        return blogDetailsPage.areEditCommentButtonsPresent();
    }

    public boolean areDeleteCommentButtonsEnabled() {
        return blogDetailsPage.areDeleteCommentButtonsEnabled();
    }

    public boolean areDeleteCommentButtonsPresent() {
        return blogDetailsPage.areDeleteCommentButtonsPresent();
    }

    public boolean areReplyCommentButtonsEnabled() {
        return blogDetailsPage.areReplyToCommentButtonsEnabled();
    }

    public boolean areReplyCommentButtonsPresent() {
        return blogDetailsPage.areReplyToCommentButtonsPresent();
    }

    public boolean areUpVoteCommentButtonsEnabled() {
        return blogDetailsPage.areUpVoteCommentButtonsEnabled();
    }

    public boolean areUpVoteCommentButtonsPresent() {
        return blogDetailsPage.areUpVoteCommentButtonsPresent();
    }

    public boolean areDownVoteCommentButtonsEnabled() {
        return blogDetailsPage.areDownVoteCommentButtonsEnabled();
    }

    public boolean areDownVoteCommentButtonsPresent() {
        return blogDetailsPage.areDownVoteCommentButtonsPresent();
    }
}
