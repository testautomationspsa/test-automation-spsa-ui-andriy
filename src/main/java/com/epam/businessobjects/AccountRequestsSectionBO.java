package com.epam.businessobjects;

import com.epam.models.TrainingRequestModel;
import com.epam.pageobjects.implementations.AccountRequestsSectionPage;

public class AccountRequestsSectionBO {
    private AccountRequestsSectionPage accountRequestsSectionPage = new AccountRequestsSectionPage();

    public TrainingRequestModel getFirstTrainingRequestModel() {
        return accountRequestsSectionPage.getFirstTrainingRequestModel();
    }

    public DoYouWantToDeletePopUpBO deleteFirstTrainingRequest() {
        accountRequestsSectionPage.clickDeleteFirstRequestButton();
        return new DoYouWantToDeletePopUpBO();
    }

    public AddTrainingRequestPopUpBO clickCreateNewRequestButton() {
        accountRequestsSectionPage.clickCreateNewRequestButton();
        return new AddTrainingRequestPopUpBO();
    }

    public AddTrainingRequestPopUpBO clickFirstEditRequestButton() {
        accountRequestsSectionPage.clickFirstEditRequestButton();
        return new AddTrainingRequestPopUpBO();
    }

    public AddTrainingRequestPopUpBO clickSaveEditedRequestButton() {
        accountRequestsSectionPage.clickSaveEditedRequestButton();
        return new AddTrainingRequestPopUpBO();
    }

    public boolean areAnyRequestsPresent() {
        return accountRequestsSectionPage.areAnyRequestsPresent();
    }
}
