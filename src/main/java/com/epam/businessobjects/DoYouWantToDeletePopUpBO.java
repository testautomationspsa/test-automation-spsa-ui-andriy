package com.epam.businessobjects;

import com.epam.pageobjects.implementations.DoYouWantToDeletePopUpPage;

public class DoYouWantToDeletePopUpBO {
    private DoYouWantToDeletePopUpPage doYouWantToDeletePopUpPage = new DoYouWantToDeletePopUpPage();

    public void clickDeleteButton() {
        doYouWantToDeletePopUpPage.clickDeleteButton();
    }

    public void clickCancelButton() {
        doYouWantToDeletePopUpPage.clickCancelButton();
    }
}
