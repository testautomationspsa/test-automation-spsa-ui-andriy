package com.epam.businessobjects;

import com.epam.pageobjects.implementations.SportBuddiesHomePage;

public class SportBuddiesMainBO {
    private SportBuddiesHomePage sportBuddiesMainPage = new SportBuddiesHomePage();

    public UserAccountBO goToUserAccountPage(){
        sportBuddiesMainPage.goToUserAccountPage();
        return new UserAccountBO();
    }

    public SportBuddiesMainBO goToSportBuddiesMainPage(){
        sportBuddiesMainPage.goToSportBuddiesMainPage();
        return this;
    }

    public BlogBO goToBlogPage(){
        sportBuddiesMainPage.clickBlogButton();
        return new BlogBO();
    }

    public LoginBO clickSignInButton(){
        sportBuddiesMainPage.clickSignInButton();
        return new LoginBO();
    }

    public SportBuddiesMainBO clickSignUpButon(){
        sportBuddiesMainPage.clickSignUpButton();
        return this;
    }

    public SportBuddiesMainBO clickSearchButon(){
        sportBuddiesMainPage.clickSearchButton();
        return this;
    }

    public FindVenueBO clickSearchLocation(){
        sportBuddiesMainPage.clickSearchLocationButton();
        return new FindVenueBO();
    }

    public FindPartnersBO clickSearchPartner(){
        sportBuddiesMainPage.clickSearchPartnerButton();
        return new FindPartnersBO();
    }

    /*public FindTrainerBO clickSearchTrainer(){
        sportBuddiesMainPage.clickSearchPartnerButton();
        return new FindTrainerBO();
    }*/

    public Boolean checkForLogoPresence(){
        return sportBuddiesMainPage.isLogoPresent();
    }

    public Boolean checkForSignInButtonPresence(){
        return sportBuddiesMainPage.isSignInButtonPresent();
    }

    public Boolean checkForSignUpButtonPresence(){
        return sportBuddiesMainPage.isSignUpButtonPresent();
    }

    public Boolean checkForSearchDropdownPresence(){
        return sportBuddiesMainPage.isSearchDopDownPresent();
    }

    public Boolean checkForUserProfileDropdownPresence(){
        return sportBuddiesMainPage.isProfileIconButtonDisplayed();
    }

    public Boolean checkForBlogButtonPresence(){
        return sportBuddiesMainPage.isBlogButtonPresent();
    }

    public boolean isOnSportBuddiesMainPage(){
        return sportBuddiesMainPage.isOnSportBuddiesMainPage();
    }


}
