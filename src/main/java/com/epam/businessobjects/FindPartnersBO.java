package com.epam.businessobjects;

import com.epam.pageobjects.implementations.FindPartnerPage;
import com.epam.utils.Waiter;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import com.epam.utils.enums.TrainingTime;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class FindPartnersBO {
    private static final Logger LOGGER = LogManager.getLogger(FindPartnersBO.class);
    private FindPartnerPage findPartnerPage = new FindPartnerPage();

    public Boolean checkSearchByGender(Gender gender) {
        if (gender == Gender.MALE) {
            findPartnerPage.checkingOutMaleCheckBox();
        } else if (gender == Gender.FEMALE) {
            findPartnerPage.checkingOutFemaleCheckBox();
        } else if (gender == Gender.ANY) {
            findPartnerPage.checkingOutBothGendersCheckBox();
        }
        findPartnerPage.checkingOutAllDayCheckBox();
        findPartnerPage.clickSubmitButton();
        Waiter.waitFor(Waiter.SMALL_WAIT);
        List<String> genderList = findPartnerPage.getPartnersGenderList();
        boolean correct = true;
        for (String genderType : genderList) {
            LOGGER.info("expected: "+gender.toString()+", came: "+genderType);
            if (!genderType.toUpperCase().equals(gender.getGenderValue())) {
                correct = false;
            }
        }
        return correct;
    }

    public Boolean checkSearchBySportType(SportType sportType) {
        findPartnerPage.clickSportTypeButton();
        if (sportType.equals(SportType.RUNNING)) {
            findPartnerPage.clickSportTypeRunningButton();
        } else if (sportType.equals(SportType.SWIMMING)) {
            findPartnerPage.clickSportTypeSwimmingButton();
        } else if (sportType.equals(SportType.FOOTBALL)) {
            findPartnerPage.clickSportTypeFootballButton();
        } else if (sportType.equals(SportType.YOGA)) {
            findPartnerPage.clickSportTypeYogaButton();
        }
        findPartnerPage.checkingOutBothGendersCheckBox();
        findPartnerPage.clickSubmitButton();
        Waiter.waitFor(Waiter.SMALL_WAIT);
        List<String> sportTypeList = findPartnerPage.getPartnersSportTypeList();
        boolean correct = true;
        for (String sport : sportTypeList) {
            LOGGER.info("expected: "+sportType.toString()+", came: "+sport);
            if (!sport.toUpperCase().equals(sportType.getSportType())) {
                correct = false;
            }
        }
        findPartnerPage.clickSportTypeButton();
        if (sportType.equals(SportType.RUNNING)) {
            findPartnerPage.clickSportTypeRunningButton();
        } else if (sportType.equals(SportType.SWIMMING)) {
            findPartnerPage.clickSportTypeSwimmingButton();
        } else if (sportType.equals(SportType.FOOTBALL)) {
            findPartnerPage.clickSportTypeFootballButton();
        } else if (sportType.equals(SportType.YOGA)) {
            findPartnerPage.clickSportTypeYogaButton();
        }
        findPartnerPage.checkingOutAllDayCheckBox();
        return correct;
    }

    public Boolean checkSearchByTime(TrainingTime trainingTime) {
        if (trainingTime == TrainingTime.MORNING) {
            findPartnerPage.checkingOutMorningCheckBox();
        } else if (trainingTime == TrainingTime.AFTERNOON) {
            findPartnerPage.checkingOutAfternoonCheckBox();
        } else if (trainingTime == TrainingTime.EVENING) {
            findPartnerPage.checkingOutEveningCheckBox();
        } else if (trainingTime == TrainingTime.ALL_DAY) {
            findPartnerPage.checkingOutAllDayCheckBox();
        }
        findPartnerPage.checkingOutBothGendersCheckBox();
        findPartnerPage.clickSubmitButton();
        Waiter.waitFor(Waiter.SMALL_WAIT);
        List<String> trainingTimeList = findPartnerPage.getPartnersTimeList();
        boolean correct = true;
        for (String time : trainingTimeList) {
            LOGGER.info("expected: "+trainingTime.toString()+", came: "+time);
            if (!time.toUpperCase().contains(trainingTime.getTrainingTime())) {
                correct = false;
            }
        }
        if (trainingTime == TrainingTime.MORNING) {
            findPartnerPage.checkingOutMorningCheckBox();
        } else if (trainingTime == TrainingTime.AFTERNOON) {
            findPartnerPage.checkingOutAfternoonCheckBox();
        } else if (trainingTime == TrainingTime.EVENING) {
            findPartnerPage.checkingOutEveningCheckBox();
        } else if (trainingTime == TrainingTime.ALL_DAY) {
            findPartnerPage.checkingOutAllDayCheckBox();
        }
        return correct;
    }

    public Boolean checkSearchByMaturityLevel(MaturityLevel maturityLevel) {
        if (maturityLevel == MaturityLevel.BEGINNER) {
            findPartnerPage.checkingOutBeginnerCheckBox();
        } else if (maturityLevel == MaturityLevel.MIDDLE) {
            findPartnerPage.checkingOutMiddleCheckBox();
        } else if (maturityLevel == MaturityLevel.PRO) {
            findPartnerPage.checkingOutProfessionalCheckBox();
        }
        findPartnerPage.checkingOutBothGendersCheckBox();
        findPartnerPage.checkingOutAllDayCheckBox();
        findPartnerPage.clickSubmitButton();
        Waiter.waitFor(Waiter.SMALL_WAIT);
        List<String> maturityLevelsList = findPartnerPage.getPartnersMaturityLevelList();
        boolean correct = true;
        for (String maturity : maturityLevelsList) {
            LOGGER.info("expected: "+maturityLevel.toString()+", came: "+maturity);
            if (!maturity.toUpperCase().equals(maturityLevel.getMaturityLevel())) {
                correct = false;
            }
        }
        if (maturityLevel == MaturityLevel.BEGINNER) {
            findPartnerPage.checkingOutBeginnerCheckBox();
        } else if (maturityLevel == MaturityLevel.MIDDLE) {
            findPartnerPage.checkingOutMiddleCheckBox();
        } else if (maturityLevel == MaturityLevel.PRO) {
            findPartnerPage.checkingOutProfessionalCheckBox();
        }
        findPartnerPage.checkingOutAllDayCheckBox();
        return correct;
    }

    public void uncheckAll() {
        findPartnerPage.uncheckAll();
    }

    public boolean isOnFindPartnersPage() {
        return findPartnerPage.isOnFindPartnersPage();
    }

    public boolean isEmailToOtherUserButtonEnabled() {
        return findPartnerPage.isEmailToOtherUserButtonsEnabled();
    }
}
