package com.epam.businessobjects;

import com.epam.pageobjects.implementations.UserAccountPage;

public class UserAccountBO {
    SportBuddiesMainBO sportBuddiesMainBO = new SportBuddiesMainBO();
    UserAccountPage userAccountPage = new UserAccountPage();

    public boolean isOnUserProfilePage() {
        return userAccountPage.isOnUserAccountPage();
    }

    public boolean checkIfProfileButtonIsPresent() {
        return userAccountPage.isProfileButtonPresent();
    }

    public boolean checkIfAchievementsButtonIsPresent() {
        return userAccountPage.isAchievementsButtonPresent();
    }

    public boolean checkIfRequestsButtonIsPresent() {
        return userAccountPage.isRequestsButtonPresent();
    }

    public AccountProfileSectionBO goToUserProfile() {
        userAccountPage.clickOnProfileButton();
        return new AccountProfileSectionBO();
    }

    public UserAccountBO goToUserAchievements() {
        userAccountPage.clickOnAchievementsButton();
        return this;
    }

    public UserAccountBO goToUserRequests() {
        userAccountPage.clickOnRequestsButton();
        return this;
    }
}
