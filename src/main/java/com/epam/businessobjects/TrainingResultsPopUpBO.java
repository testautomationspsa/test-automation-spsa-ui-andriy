package com.epam.businessobjects;

import com.epam.pageobjects.implementations.TrainingResultsPopUpPage;

public class TrainingResultsPopUpBO {
    private TrainingResultsPopUpPage trainingResultsPopUpPage = new TrainingResultsPopUpPage();

    public void clickOkButton() {
        trainingResultsPopUpPage.clickOkButton();
    }

    public void clickCancelButton() {
        trainingResultsPopUpPage.clickCancelButton();
    }
}
