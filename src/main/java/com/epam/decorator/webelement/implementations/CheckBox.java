package com.epam.decorator.webelement.implementations;

import com.epam.utils.CustomActions;
import com.epam.utils.DriverManager;
import com.epam.utils.Waiter;
import com.epam.decorator.webelement.BasicWebElement;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class CheckBox extends BasicWebElement {
    public CheckBox(WebElement WEBElement) {
        super(WEBElement);
    }

    public void setCheckBoxSelected() {
        CustomActions.scrollToElement(DriverManager.getDriver(),webElement);
        Waiter.waitForElementToBeClickable(webElement, Waiter.LONG_WAIT);
        if (!webElement.isSelected()) {
            webElement.click();
        }
    }

    public void setCheckBoxUnSelected() {
        CustomActions.scrollToElement(DriverManager.getDriver(),webElement);
        Waiter.waitForElementToBeClickable(webElement, Waiter.LONG_WAIT);
        if (webElement.isSelected()) {
            webElement.click();
        }
    }

    public boolean isSelected() {
        Waiter.waitForElementToBeClickable(webElement, Waiter.SMALL_WAIT);
        return webElement.isSelected();
    }

    public boolean isDisplayed(){
        boolean isDisplayed;
        try {
            isDisplayed = webElement.isDisplayed();
        } catch (NoSuchElementException e){
            isDisplayed=false;
        }
        return isDisplayed;
    }
}
