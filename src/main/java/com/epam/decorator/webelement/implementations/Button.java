package com.epam.decorator.webelement.implementations;

import com.epam.utils.DriverManager;
import com.epam.utils.Waiter;
import com.epam.decorator.webelement.BasicWebElement;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Button extends BasicWebElement {
    public Button(WebElement WEBElement) {
        super(WEBElement);
    }

    public void click() {
        Waiter.waitForElementToBeClickable(webElement,Waiter.LONG_WAIT);
        webElement.click();
    }

    public boolean isEnabled() {
        Waiter.waitForVisibilityOfElement(webElement,Waiter.MEDIUM_WAIT);
        return webElement.isEnabled();
    }

    public void doubleClick() {
        Waiter.waitForVisibilityOfElement(webElement, Waiter.SMALL_WAIT);
        new Actions(DriverManager.getDriver()).doubleClick(webElement);
    }

    public boolean isDisplayed(){
        boolean isDisplayed;
        try {
            isDisplayed = webElement.isDisplayed();
        } catch (NoSuchElementException e){
            isDisplayed=false;
        }
        return isDisplayed;
    }
}
