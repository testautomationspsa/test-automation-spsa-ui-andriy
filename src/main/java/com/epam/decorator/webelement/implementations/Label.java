package com.epam.decorator.webelement.implementations;

import com.epam.utils.Waiter;
import com.epam.decorator.webelement.BasicWebElement;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class Label extends BasicWebElement {
    public Label(WebElement WEBElement) {
        super(WEBElement);
    }

    public String getText(){
        Waiter.waitForVisibilityOfElement(webElement, Waiter.MEDIUM_WAIT);
        return webElement.getText();
    }

    public boolean isDisplayed(){
        boolean isDisplayed;
        try {
            isDisplayed = webElement.isDisplayed();
        } catch (NoSuchElementException e){
            isDisplayed=false;
        }
        return isDisplayed;
    }
}
