package com.epam.decorator.webelement.implementations;

import com.epam.utils.Waiter;
import com.epam.decorator.webelement.BasicWebElement;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class InputTextField extends BasicWebElement {
    public InputTextField(WebElement WEBElement) {
        super(WEBElement);
    }

    public void sendKeys(CharSequence... charSequences) {
        Waiter.waitForVisibilityOfElement(webElement, Waiter.LONG_WAIT);
        webElement.clear();
        webElement.sendKeys(charSequences);
    }

    public String getText() {
        Waiter.waitForVisibilityOfElement(webElement, Waiter.SMALL_WAIT);
        return webElement.getText();
    }

    public void click() {
        Waiter.waitForVisibilityOfElement(webElement, Waiter.SMALL_WAIT);
        webElement.click();
    }

    public boolean isEnabled() {
        Waiter.waitForVisibilityOfElement(webElement, Waiter.SMALL_WAIT);
        return webElement.isEnabled();
    }

    public boolean isDisplayed(){
        boolean isDisplayed;
        try {
            isDisplayed = webElement.isDisplayed();
        } catch (NoSuchElementException e){
            isDisplayed=false;
        }
        return isDisplayed;
    }
}
