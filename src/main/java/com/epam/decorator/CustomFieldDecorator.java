package com.epam.decorator;

import com.epam.decorator.webelement.BasicWebElement;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.CheckBox;
import com.epam.decorator.webelement.implementations.InputTextField;
import com.epam.decorator.webelement.implementations.Label;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.lang.reflect.*;
import java.util.List;
import java.util.Objects;

public class CustomFieldDecorator extends DefaultFieldDecorator {
    public CustomFieldDecorator(ElementLocatorFactory factory) {
        super(factory);
    }

    @Override
    public Object decorate(ClassLoader loader, Field field) {
        ElementLocator locator = factory.createLocator(field);

        if (Button.class.isAssignableFrom(field.getType())) {
            return new Button(proxyForLocator(loader, locator));
        } else if (InputTextField.class.isAssignableFrom(field.getType())) {
            return new InputTextField(proxyForLocator(loader, locator));
        } else if (CheckBox.class.isAssignableFrom(field.getType())) {
            return new CheckBox(proxyForLocator(loader, locator));
        } else if (Label.class.isAssignableFrom(field.getType())) {
            return new Label(proxyForLocator(loader, locator));
        }else if (List.class.isAssignableFrom(field.getType())) {
            Class<BasicWebElement> decoratableClass = decoratableClass(field);
            return createList(loader, locator, decoratableClass);
        } else if (BasicWebElement.class.isAssignableFrom(field.getType())) {
            return new BasicWebElement(proxyForLocator(loader, locator));
        } else {
            return super.decorate(loader, field);
        }
    }

    @SuppressWarnings("unchecked")
    private List<BasicWebElement> createList(ClassLoader loader, ElementLocator locator, Class<BasicWebElement> clazz) {
        InvocationHandler handler = new CustomElementListHandler(locator, clazz);
        return (List<BasicWebElement>) Proxy.newProxyInstance(
                loader, new Class[]{List.class}, handler);
    }

    @SuppressWarnings("unchecked")
    private Class<BasicWebElement> decoratableClass(Field field) {
        Class<?> clazz = field.getType();
        if (List.class.isAssignableFrom(clazz)) {
            if (Objects.isNull(field.getAnnotation(FindBy.class)) &&
                    Objects.isNull(field.getAnnotation(FindBys.class))) {
                return null;
            }
            Type genericType = field.getGenericType();
            if (!(genericType instanceof ParameterizedType)) {
                return null;
            }
            clazz = (Class<?>) ((ParameterizedType) genericType).
                    getActualTypeArguments()[0];
        }
        if (BasicWebElement.class.isAssignableFrom(clazz)) {
            return (Class<BasicWebElement>) clazz;
        } else {
            return null;
        }
    }
}

