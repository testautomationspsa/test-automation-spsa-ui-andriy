package com.epam.utils.enums;

public enum MaturityLevel {
    BEGINNER("BEGINNER"),MIDDLE("MIDDLE"),PRO("PRO");

    private String maturityLevel;

    MaturityLevel(String maturityLevel){
        this.maturityLevel=maturityLevel;
    }

    public String getMaturityLevel() {
        return maturityLevel;
    }
}
