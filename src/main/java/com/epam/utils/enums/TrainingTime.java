package com.epam.utils.enums;

public enum  TrainingTime {
    MORNING("MORNING;"), AFTERNOON("AFTERNOON;"),EVENING("EVENING;"),ALL_DAY("MORNING;AFTERNOON;EVENING;");

    private String time;

    TrainingTime(String time){
        this.time=time;
    }

    public String getTrainingTime() {
        return time;
    }
}
