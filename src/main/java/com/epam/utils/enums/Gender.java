package com.epam.utils.enums;

public enum Gender {
    MALE("MALE"),FEMALE("FEMALE"), ANY("ANY");

    String genderValue;

    Gender(String genderValue){
        this.genderValue = genderValue;
    }

    public String getGenderValue(){
        return genderValue;
    }
}
