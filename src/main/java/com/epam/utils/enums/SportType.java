package com.epam.utils.enums;

public enum  SportType {
    RUNNING("RUNNING"),SWIMMING("SWIMMING"),FOOTBALL("FOOTBALL"),YOGA("YOGA");

    private String sportType;

    SportType(String sportType){
        this.sportType=sportType;
    }

    public String getSportType() {
        return sportType;
    }
}
