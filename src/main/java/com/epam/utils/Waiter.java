package com.epam.utils;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {
    public static final int MICRO_WAIT = 1;
    public static final int SMALL_WAIT = 10;
    public static final int MEDIUM_WAIT = 30;
    public static final int LONG_WAIT = 60;

    private Waiter() {
    }

    public static WebElement waitForVisibilityOfElement(WebElement element, int time) {
        return (new WebDriverWait(DriverManager.getDriver(), time))
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement waitForElementToBeClickable(WebElement element, int time) {
        return (new WebDriverWait(DriverManager.getDriver(), time)).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitFor(int seconds){
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
