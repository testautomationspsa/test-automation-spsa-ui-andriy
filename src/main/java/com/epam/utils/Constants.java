package com.epam.utils;

public class Constants {
    public static final String SPORT_BUDDIES_URI = "https://sportbuddies-ui.herokuapp.com";

    private Constants(){}
}
