package com.epam.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CustomActions {
    public static void scrollToElement(WebDriver driver, WebElement webElement) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(" + webElement.getLocation().x + ","
                + (webElement.getLocation().y - 30) + ")");
    }

    public static void scrollToTop(WebDriver driver) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, -document.body.scrollHeight)");
    }

    public static void scrollToBottom(WebDriver driver) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public static String getHiddenAttributeValue(WebDriver driver, WebElement webElement) {
        return (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].value",
                webElement);
    }

    public static void refreshPage(WebDriver driver) {
        driver.navigate().refresh();
    }

    public static void clickOnBody(WebDriver driver) {
        driver.findElement(By.xpath("//body")).click();
    }
}
