package com.epam.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class DriverManager {
    private static final ThreadLocal<WebDriver> driver_pool = new ThreadLocal<>();

    private DriverManager() {
    }

    public static WebDriver getDriver() {
        if (Objects.isNull(driver_pool.get())) {
            WebDriverManager.chromedriver().setup();
            WebDriver driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver_pool.set(driver);
        }
        return driver_pool.get();
    }

    public static void setDriver(WebDriver driver) {
        driver_pool.set(driver);
    }

    public static void quitDriver() {
        if (Objects.nonNull(driver_pool.get())) {
            driver_pool.get().quit();
        }
    }
}
