package com.epam.utils;

import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import org.testng.collections.Maps;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class CustomSoftAssert extends Assertion {
    private static final Logger LOGGER = LogManager.getLogger(CustomSoftAssert.class);
    private final Map<AssertionError, IAssert<?>> errorsMap = Maps.newLinkedHashMap();
    private String assertMessage = null;

    private static void writeStep(String message, Status status) {
        String uuid = UUID.randomUUID().toString();
        StepResult result = new StepResult().withName(message).withStatus(status);
        Allure.getLifecycle().startStep(uuid, result);
        Allure.getLifecycle().stopStep(uuid);
    }

    @Override
    protected void doAssert(IAssert<?> iAssert) {
        onBeforeAssert(iAssert);
        try {
            assertMessage = iAssert.getMessage();
            iAssert.doAssert();
            onAssertSuccess(iAssert);
        } catch (AssertionError ex) {
            onAssertFailure(iAssert, ex);
            errorsMap.put(ex, iAssert);
            saveScreenshot(assertMessage);
        } finally {
            onAfterAssert(iAssert);
        }
    }

    @Override
    public void onAssertFailure(IAssert iAssert, AssertionError ae) {
        writeStep("[FAILED] " + assertMessage, Status.FAILED);
    }

    public void assertAll() {
        if (!errorsMap.isEmpty()) {
            StringBuilder sb = new StringBuilder("The following asserts failed:");
            for (Map.Entry<AssertionError, IAssert<?>> ae : errorsMap.entrySet()) {
                sb.append("\n\t");
                sb.append("(" + ae.getKey().getMessage()
                        .replaceAll("did not expect to find \\[true\\] but found \\[false\\]", "") + ")");
            }
            LOGGER.info(sb.toString());
            throw new AssertionError(sb.toString());
        }
    }

    @Attachment(value = "pageScreenshot_{1}", type = "image/png")
    private byte[] saveScreenShotPNG(WebDriver driver, String name) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    public void saveScreenshot(String assertMessage) {
        WebDriver driver = DriverManager.getDriver();
        if (Objects.nonNull(driver)) {
            LOGGER.info("Screenshot captured for failed testcase");
            saveScreenShotPNG(driver, assertMessage.replaceAll("\\s", ""));
        }
    }
}
