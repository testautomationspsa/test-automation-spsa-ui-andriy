package com.epam.pageobjects;

import com.epam.decorator.CustomFieldDecorator;
import com.epam.utils.DriverManager;
import com.epam.decorator.webelement.BasicWebElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

import java.util.List;

public class AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(AbstractPageObject.class);
    protected WebDriver driver;

    protected AbstractPageObject() {
        driver = DriverManager.getDriver();
        PageFactory.initElements(new CustomFieldDecorator(
                new DefaultElementLocatorFactory(DriverManager.getDriver())), this);
    }

    protected boolean areWebElementsPresent(List<? extends BasicWebElement> webElements) {
        return !webElements.isEmpty();
    }

    protected boolean areWebElementsEnabled(List<? extends BasicWebElement> webElements) {
        boolean areEnabled = true;
        for (BasicWebElement edit : webElements) {
            if (!edit.isEnabled()) {
                areEnabled = false;
                break;
            }
        }
        return areEnabled;
    }
}
