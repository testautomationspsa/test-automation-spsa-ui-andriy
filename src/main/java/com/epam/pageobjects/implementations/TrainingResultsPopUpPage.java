package com.epam.pageobjects.implementations;

import com.epam.decorator.webelement.implementations.Button;
import com.epam.pageobjects.AbstractPageObject;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class TrainingResultsPopUpPage extends AbstractPageObject {
    //span[text()='Ok']/parent::button
    private static final Logger LOGGER = LogManager.getLogger(TrainingResultsPopUpPage.class);
    @FindBy(xpath = "//span[text()='Ok']/parent::button")
    private Button okButton;
    @FindBy(xpath = "//span[text()='Cancel']/parent::button")
    private Button cancelButton;

    @Step("clicking Ok Button")
    public void clickOkButton() {
        LOGGER.info("clicking Ok Button");
        okButton.click();
    }

    @Step("clicking cancel Button")
    public void clickCancelButton() {
        LOGGER.info("clicking cancel Button");
        cancelButton.click();
    }
}
