package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.CheckBox;
import com.epam.decorator.webelement.implementations.InputTextField;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class AddTrainingRequestPopUpPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(AddTrainingRequestPopUpPage.class);
    @FindBy(xpath = "//mat-select[@formcontrolname='sportType']")
    private Button sportTypeButton;
    @FindBy(xpath = "//span[contains(text(),'Running')]/parent::mat-option")
    private Button sportTypeRunning;
    @FindBy(xpath = "//input[@formcontrolname='minDistance']")
    private InputTextField minRunningDistance;
    @FindBy(xpath = "//input[@formcontrolname='maxDistance']")
    private InputTextField maxRunningDistance;
    @FindBy(xpath = "//span[contains(text(),'Swimming')]/parent::mat-option")
    private Button sportTypeSwimming;
    @FindBy(xpath = "//span[contains(text(),'Football')]/parent::mat-option")
    private Button sportTypeFootball;
    @FindBy(xpath = "//span[contains(text(),'Yoga')]/parent::mat-option")
    private Button sportTypeYoga;
    @FindBy(xpath = "//*[@id='morning']")
    private CheckBox morningCheckBox;
    @FindBy(xpath = "//*[@id='afternoon']")
    private CheckBox afternoonCheckBox;
    @FindBy(xpath = "//*[@id='evening']")
    private CheckBox eveningCheckBox;
    @FindBy(xpath = "//*[@id='all']")
    private CheckBox allDayCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Male')]")
    private CheckBox maleCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Female')]")
    private CheckBox femaleCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Both')]")
    private CheckBox bothGendersCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Beginner')]")
    private CheckBox beginnerCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Middle')]")
    private CheckBox middleCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Professional')]")
    private CheckBox professionalCheckBox;
    @FindBy(xpath = "//button/child::span[text()='CREATE']")
    private Button createButton;

    @Step("clicking sportType button")
    public AddTrainingRequestPopUpPage clickSportTypeButton() {
        LOGGER.info("clicking sportType button");
        sportTypeButton.click();
        return this;
    }

    @Step("clicking sportType RUNNING  button")
    public AddTrainingRequestPopUpPage clickSportTypeRunningButton() {
        LOGGER.info("clicking sportType RUNNING button");
        sportTypeRunning.click();
        return this;
    }

    @Step("filling MinRunningDistance")
    public AddTrainingRequestPopUpPage fillMinRunningDistance(int distance) {
        LOGGER.info("filling MinRunningDistance");
        minRunningDistance.click();
        minRunningDistance.sendKeys(String.valueOf(distance));
        return this;
    }

    @Step("filling MaxRunningDistance")
    public AddTrainingRequestPopUpPage fillMaxRunningDistance(int distance) {
        LOGGER.info("filling MaxRunningDistance");
        maxRunningDistance.click();
        maxRunningDistance.sendKeys(String.valueOf(distance));
        return this;
    }

    @Step("clicking sportType SWIMMING  button")
    public AddTrainingRequestPopUpPage clickSportTypeSwimmingButton() {
        LOGGER.info("clicking sportType SWIMMING button");
        sportTypeSwimming.click();
        return this;
    }

    @Step("clicking sportType FOOTBALL  button")
    public AddTrainingRequestPopUpPage clickSportTypeFootballButton() {
        LOGGER.info("clicking sportType FOOTBALL button");
        sportTypeFootball.click();
        return this;
    }

    @Step("clicking sportType YOGA  button")
    public AddTrainingRequestPopUpPage clickSportTypeYogaButton() {
        LOGGER.info("clicking sportType YOGA button");
        sportTypeYoga.click();
        return this;
    }

    @Step("checking Out Morning CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutMorningCheckBox() {
        LOGGER.info("checking Out Morning CheckBox.");
        morningCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Afternoon CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutAfternoonCheckBox() {
        LOGGER.info("checking Out Afternoon CheckBox.");
        afternoonCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Evening CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutEveningCheckBox() {
        LOGGER.info("checking Out Evening CheckBox.");
        eveningCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out All Day CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutAllDayCheckBox() {
        LOGGER.info("checking Out All Day CheckBox.");
        allDayCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Male CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutMaleCheckBox() {
        LOGGER.info("checking Out Male CheckBox.");
        maleCheckBox.click();
        return this;
    }

    @Step("checking Out Female CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutFemaleCheckBox() {
        LOGGER.info("checking Out Female CheckBox.");
        femaleCheckBox.click();
        return this;
    }

    @Step("checking Out Both Genders CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutBothGendersCheckBox() {
        LOGGER.info("checking Out Both Genders CheckBox.");
        bothGendersCheckBox.click();
        return this;
    }

    @Step("checking Out Beginner CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutBeginnerCheckBox() {
        LOGGER.info("checking Out Beginner CheckBox.");
        beginnerCheckBox.click();
        beginnerCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Middle CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutMiddleCheckBox() {
        LOGGER.info("checking Out Middle CheckBox.");
        middleCheckBox.click();
        middleCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Professional CheckBox.")
    public AddTrainingRequestPopUpPage checkingOutProfessionalCheckBox() {
        LOGGER.info("checking Out Professional CheckBox.");
        professionalCheckBox.click();
        professionalCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("clicking Submit Button.")
    public AddTrainingRequestPopUpPage clickSubmitButton() {
        LOGGER.info("clicking Submit Button.");
        createButton.click();
        return this;
    }
}
