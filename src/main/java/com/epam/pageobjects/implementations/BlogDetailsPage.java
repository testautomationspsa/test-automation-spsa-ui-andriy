package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.utils.CustomActions;
import com.epam.utils.DriverManager;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.InputTextField;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class BlogDetailsPage extends AbstractPageObject {
    private static final String BLOG_DETAILS_PAGE_URI = "https://SportBuddies.com/blog/";
    private static final Logger LOGGER = LogManager.getLogger(BlogDetailsPage.class);
    @FindBy(xpath = "//*[contains(@class,'post__header')]")
    private Label BlogPostName;
    @FindBy(xpath = "//a[@class='post__tag ng-star-inserted']")
    private List<Button> blogPostTags;
    @FindBy(xpath = "//button/child::span[contains(text(),'COMMENT')]")
    private Button postCommentButton;
    @FindBy(xpath = "//*[contains(text(),'BACK')]")
    private Button goBackButton;
    @FindBy(xpath = "//div[@class='comments__text ng-star-inserted']")
    private List<Label> comments;
    @FindBy(xpath = "//textarea[@placeholder='Text of your comment']")
    private InputTextField commentField;
    @FindBy(xpath = "//textarea[not(@placeholder='Text of your comment')]")
    private InputTextField editCommentField;
    @FindBy(xpath = "//button/child::span[contains(text(),'EDIT')]")
    private List<Button> editCommentButtons;
    @FindBy(xpath = "//span[contains(text(),'DELETE')]/parent::button")
    private List<Button> deleteCommentButtons;
    @FindBy(xpath = "//button/child::span[contains(text(),'REPLY')]")
    private List<Button> replyCommentButtons;
    @FindBy(xpath = "//button/child::span[contains(text(),'EDIT COMMENT')]")
    private Button postEditedCommentButton;
    @FindBy(xpath = "//button[@class='material-icons comments__upvote-img mat-icon-button mat-button-base']")
    private List<Button> upVoteCommentButtons;
    @FindBy(xpath = "//button[@class='material-icons comments__downvote-img mat-icon-button mat-button-base']")
    private List<Button> downVoteCommentButtons;
    @FindBy(xpath = "//span[@class='comments__upvote-counter']")
    private List<Label> upVoteCommentCounter;
    @FindBy(xpath = "//span[@class='comments__downvote-counter']")
    private List<Label> downVoteCommentCounter;

    @Step("isOnBlogDetailsPage check")
    public boolean isOnBlogDetailsPage() {
        return driver.getCurrentUrl().contains(BLOG_DETAILS_PAGE_URI);
    }

    @Step("clicking first edit Comment Button")
    public BlogDetailsPage clickFirstEditCommentButton() {
        LOGGER.info("clicking first edit Comment Button");
        editCommentButtons.get(0).click();
        return this;
    }

    @Step("clicking first Up Vote Comment Button")
    public BlogDetailsPage clickFirstUpVoteCommentButton() {
        LOGGER.info("clicking first Up Vote Comment Button");
        upVoteCommentButtons.get(0).click();
        return this;
    }

    @Step("clicking first Down Vote Comment Button")
    public BlogDetailsPage clickFirstDownVoteCommentButton() {
        LOGGER.info("clicking first Down Vote Comment Button");
        downVoteCommentButtons.get(0).click();
        return this;
    }

    @Step("getting first comment Up Vote Counter")
    public Integer getFirstCommentUpVoteCounter() {
        LOGGER.info("getting first comment Up Vote Counter");
        return Integer.valueOf(upVoteCommentCounter.get(0).getText().trim());
    }

    @Step("getting first comment Down Vote Counter")
    public Integer getFirstCommentDownVoteCounter() {
        LOGGER.info("getting first comment Down Vote Counter");
        return Integer.valueOf(downVoteCommentCounter.get(0).getText().trim());
    }

    @Step("clicking post edited Comment Button")
    public BlogDetailsPage clickPostEditedCommentButton() {
        LOGGER.info("clicking post edited Comment Button");
        postEditedCommentButton.click();
        return this;
    }

    @Step("clicking first delete Comment Button")
    public BlogDetailsPage clickFirstDeleteCommentButton() {
        LOGGER.info("clicking first delete Comment Button");
        deleteCommentButtons.get(0).click();
        return this;
    }

    @Step("clicking first reply to Comment Button")
    public BlogDetailsPage clickFirstReplyCommentButton() {
        LOGGER.info("clicking first reply to Comment Button");
        replyCommentButtons.get(0).click();
        return this;
    }

    @Step("clicking Post Comment Button")
    public BlogDetailsPage clickPostCommentButton() {
        LOGGER.info("clicking Post Comment Button");
        postCommentButton.click();
        return this;
    }

    @Step("getting first Comment text")
    public String getFirstCommentText() {
        LOGGER.info("getting first Comment text");
        return comments.get(0).getText();
    }

    @Step("verify is Post Comment Button Present")
    public boolean isPostCommentButtonPresent() {
        LOGGER.info("verify is Post Comment Button Present");
        boolean isPresent = true;
        try {
            postCommentButton.clear();
        } catch (NoSuchElementException e) {
            isPresent = false;
        }
        return isPresent;
    }

    @Step("verify are Edit Comment Buttons Present")
    public boolean areEditCommentButtonsPresent() {
        LOGGER.info("verify are Edit Comment Button Present");
        return areWebElementsPresent(editCommentButtons);
    }

    @Step("verifying if edit comment Buttons are enabled")
    public boolean areEditCommentButtonsEnabled() {
        LOGGER.info("verifying if edit comment Buttons are enabled");
        return areWebElementsEnabled(editCommentButtons);
    }

    @Step("verify are Delete Comment Buttons Present")
    public boolean areDeleteCommentButtonsPresent() {
        LOGGER.info("verify are Delete Comment Button Present");
        return areWebElementsPresent(deleteCommentButtons);
    }

    @Step("verifying if Delete comment Buttons are enabled")
    public boolean areDeleteCommentButtonsEnabled() {
        LOGGER.info("verifying if Delete comment Buttons are enabled");
        return areWebElementsEnabled(deleteCommentButtons);
    }

    @Step("verify are Reply to Comment Buttons Present")
    public boolean areReplyToCommentButtonsPresent() {
        LOGGER.info("verify are Reply to Comment Button Present");
        return areWebElementsPresent(replyCommentButtons);
    }

    @Step("verifying if Reply to comment Buttons are enabled")
    public boolean areReplyToCommentButtonsEnabled() {
        LOGGER.info("verifying if Reply to comment Buttons are enabled");
        return areWebElementsEnabled(replyCommentButtons);
    }

    @Step("verify are Up Vote Comment Buttons Present")
    public boolean areUpVoteCommentButtonsPresent() {
        LOGGER.info("verify are Up Vote Comment Button Present");
        return areWebElementsPresent(upVoteCommentButtons);
    }

    @Step("verifying if Up Vote comment Buttons are enabled")
    public boolean areUpVoteCommentButtonsEnabled() {
        LOGGER.info("verifying if Up Vote comment Buttons are enabled");
        return areWebElementsEnabled(upVoteCommentButtons);
    }

    @Step("verify are Down Vote Comment Buttons Present")
    public boolean areDownVoteCommentButtonsPresent() {
        LOGGER.info("verify are Down Vote Comment Button Present");
        return areWebElementsPresent(downVoteCommentButtons);
    }

    @Step("verifying if Down Vote comment Buttons are enabled")
    public boolean areDownVoteCommentButtonsEnabled() {
        LOGGER.info("verifying if Down Vote comment Buttons are enabled");
        return areWebElementsEnabled(downVoteCommentButtons);
    }

    @Step("verifying is Post Comment Button Enabled")
    public boolean isPostCommentButtonEnabled() {
        LOGGER.info("verify is Post Comment Button Enabled");
        return postCommentButton.isEnabled();
    }

    @Step("sending comment'{0}' to comment field")
    public BlogDetailsPage writeComment(String comment) {
        LOGGER.info("sending comment'" + comment + "' to comment field");
        CustomActions.scrollToElement(DriverManager.getDriver(),commentField.getAsWebElement());
        commentField.click();
        commentField.sendKeys(comment);
        return this;
    }

    @Step("sending comment'{0}' to edit comment field")
    public BlogDetailsPage editComment(String comment) {
        LOGGER.info("sending comment'" + comment + "' to edit comment field");
        editCommentField.click();
        editCommentField.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        editCommentField.sendKeys(comment);
        return this;
    }

    @Step("clicking on first tag")
    public BlogsPage clickOnFirstTag() {
        LOGGER.info("clicking on first tag");
        if (!blogPostTags.isEmpty()) {
            blogPostTags.get(0).click();
        }
        return new BlogsPage();
    }

    @Step("clicking go back Button")
    public BlogsPage clickGoBackButton() {
        LOGGER.info("clicking go back Button");
        CustomActions.scrollToBottom(DriverManager.getDriver());
        goBackButton.click();
        return new BlogsPage();
    }
}
