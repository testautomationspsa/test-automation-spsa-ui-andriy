package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.InputTextField;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPageObject {
    private static final String LOGIN_PAGE_URI = "https://SportBuddies.com/login";
    private static final Logger LOGGER = LogManager.getLogger(LoginPage.class);
    @FindBy(xpath = "//input[contains(@placeholder,'username')]")
    private InputTextField loginField;
    @FindBy(xpath = "//input[contains(@type,'password')]")
    private InputTextField passwordField;
    @FindBy(xpath = "//*[contains(text(),'SUBMIT')]")
    private Button submitButton;
    @FindBy(xpath = "//p[contains(text(),'Invalid email or password')]")
    private Label incorrectCredentialsErrorMessage;

    @Step("going to SportBuddies Start Page")
    public void goToSportBuddiesLoginPage() {
        LOGGER.info("going to SportBuddies Start Page");
        driver.get(LOGIN_PAGE_URI);
    }

    @Step("filling Login Field with '{0}'")
    public void fillLoginField(String login) {
        LOGGER.info("filling Login Field with '" + login + "'");
        loginField.click();
        loginField.sendKeys(login);
    }

    @Step("filling Password Field with '{0}'")
    public void fillPasswordField(String password) {
        LOGGER.info("filling Password Field with '" + password + "'");
        passwordField.click();
        passwordField.sendKeys(password);
    }

    @Step("submitting Login and Password")
    public void submitLoginAndPassword() {
        LOGGER.info("submitting Login and Password");
        submitButton.click();
    }

    @Step("verifying if Login field is displayed.")
    public boolean isLoginFieldDisplayed() {
        LOGGER.info("verifying if Login field is displayed.");
        return loginField.isDisplayed();
    }

    public boolean isOnLoginPage() {
        return driver.getCurrentUrl().contains(LOGIN_PAGE_URI);
    }

    @Step("verifying if incorrect credentials error message is displayed.")
    public boolean isErrorMessageDisplayed() {
        LOGGER.info("verifying if incorrect credentials error message is displayed.");
        return incorrectCredentialsErrorMessage.isDisplayed();
    }
}
