package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.utils.CustomActions;
import com.epam.utils.DriverManager;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.InputTextField;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class AccountProfileSectionPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(AccountProfileSectionPage.class);
    @FindBy(xpath = "//input[contains(@formcontrolname,'email')]")
    private InputTextField email;
    @FindBy(xpath = "//input[contains(@formcontrolname,'firstName')]")
    private InputTextField firstName;
    @FindBy(xpath = "//input[contains(@formcontrolname,'secondName')]")
    private InputTextField lastName;
    @FindBy(xpath = "//span[text()='Female' or text()='Male']")
    private Label genderLabel;
    @FindBy(xpath = "//*[@class='mat-select-arrow']")
    private Button chooseGenderButton;
    @FindBy(xpath = "//mat-option[@value='MALE']")
    private Button chooseMaleGenderButton;
    @FindBy(xpath = "//mat-option[@value='FEMALE']")
    private Button chooseFemaleGenderButton;
    @FindBy(xpath = "//input[@formcontrolname='location']")
    private InputTextField location;
    @FindBy(xpath = "//input[contains(@formcontrolname,'dateOfBirth')]")
    private InputTextField birthDate;
    @FindBy(xpath = "//*[contains(text(),'SAVE')]")
    private Button saveButton;
    @FindBy(xpath = "//*[contains(text(),'Change your picture')]")
    private Button changeYourPictureButton;
    @FindBy(xpath = "//div[@class='pac-item']")
    private List<Button> googleMapsSuggestions;

    @Step("filling email Field with '{0}'")
    public void fillEmailField(String email) {
        LOGGER.info("filling email Field with '" + email + "'");
        this.email.click();
        this.email.sendKeys(email);
    }

    @Step("filling first Name Field with '{0}'")
    public void fillFirstNameField(String firstName) {
        LOGGER.info("filling first Name Field with '" + firstName + "'");
        this.firstName.click();
        this.firstName.sendKeys(firstName);
    }

    @Step("filling last Name Field with '{0}'")
    public void fillLastNameField(String lastName) {
        LOGGER.info("filling last Name Field with '" + lastName + "'");
        this.lastName.click();
        this.lastName.sendKeys(lastName);
    }

    @Step("choosing gender Field: '{0}'")
    public void chooseGenderField(String gender) {
        LOGGER.info("choosing gender Field: '" + gender.toUpperCase() + "'");
        chooseGenderButton.click();
        if (gender.toUpperCase().equals("MALE")) {
            chooseMaleGenderButton.click();
        } else {
            chooseFemaleGenderButton.click();
        }
    }

    @Step("filling location Field with '{0}'")
    public void fillLocationField(String location) {
        LOGGER.info("filling location Field with '" + location + "'");
        this.location.click();
        this.location.sendKeys(location);
        googleMapsSuggestions.get(0).click();
    }

    @Step("filling birthDate Field with '{0}'")
    public void fillBirthDateField(String birthDate) {
        LOGGER.info("filling birthDate Field with '" + birthDate + "'");
        this.birthDate.click();
        this.birthDate.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
        this.birthDate.sendKeys(birthDate);
    }

    @Step("submitting profile information")
    public void clickSaveButton() {
        LOGGER.info("submitting profile information");
        saveButton.click();
    }

    @Step("getting email")
    public String getEmail() {
        LOGGER.info("getting email");
        CustomActions.scrollToElement(DriverManager.getDriver(),email);
        return CustomActions.getHiddenAttributeValue(driver,email.getAsWebElement());
    }

    @Step("getting first name")
    public String getFirstName() {
        LOGGER.info("getting first name");
        CustomActions.scrollToElement(DriverManager.getDriver(),firstName);
        return CustomActions.getHiddenAttributeValue(driver,firstName.getAsWebElement());
    }

    @Step("getting last name")
    public String getLastName() {
        LOGGER.info("getting last name");
        CustomActions.scrollToElement(DriverManager.getDriver(),lastName);
        return CustomActions.getHiddenAttributeValue(driver,lastName.getAsWebElement());
    }

    @Step("getting gender")
    public String getGender() {
        LOGGER.info("getting gender");
        CustomActions.scrollToElement(DriverManager.getDriver(),genderLabel);
        return genderLabel.getText();
    }

    @Step("getting location")
    public String getLocation() {
        LOGGER.info("getting location");
        CustomActions.scrollToElement(DriverManager.getDriver(),location);
        return CustomActions.getHiddenAttributeValue(driver,location.getAsWebElement());
    }

    @Step("getting birth date")
    public String getBirthDate() {
        LOGGER.info("getting birth date");
        CustomActions.scrollToElement(DriverManager.getDriver(),birthDate);
        return CustomActions.getHiddenAttributeValue(driver,birthDate.getAsWebElement());
    }

    @Step("verifying is email displayed.")
    public Boolean isEmailDisplayed() {
        LOGGER.info("verifying is email displayed.");
        return email.isDisplayed();
    }

    @Step("verifying is firstName displayed.")
    public Boolean isFirstNameDisplayed() {
        LOGGER.info("verifying is firstName displayed.");
        return firstName.isDisplayed();
    }

    @Step("verifying is lastName displayed.")
    public Boolean isLastNameDisplayed() {
        LOGGER.info("verifying is lastName displayed.");
        return lastName.isDisplayed();
    }

    @Step("verifying is gender displayed.")
    public Boolean isGenderDisplayed() {
        LOGGER.info("verifying is gender displayed.");
        return genderLabel.isDisplayed();
    }

    @Step("verifying is location displayed.")
    public Boolean isLocationDisplayed() {
        LOGGER.info("verifying is location displayed.");
        return location.isDisplayed();
    }

    @Step("verifying is birth date displayed.")
    public Boolean isBirthDateDisplayed() {
        LOGGER.info("verifying is birth date displayed.");
        return birthDate.isDisplayed();
    }
}
