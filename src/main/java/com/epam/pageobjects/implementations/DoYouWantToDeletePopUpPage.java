package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.decorator.webelement.implementations.Button;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class DoYouWantToDeletePopUpPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(DoYouWantToDeletePopUpPage.class);
    @FindBy(xpath = "//button[contains(@class,'dialog__confirm')]")
    private Button deleteButton;
    @FindBy(xpath = "//button[contains(@class,'dialog__cancel')]")
    private Button cancelButton;

    @Step("clicking delete Button")
    public void clickDeleteButton() {
        LOGGER.info("clicking delete Button");
        deleteButton.click();
    }

    @Step("clicking cancel Button")
    public void clickCancelButton() {
        LOGGER.info("clicking cancel Button");
        cancelButton.click();
    }
}
