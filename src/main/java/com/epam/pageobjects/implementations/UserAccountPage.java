package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class UserAccountPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(UserAccountPage.class);
    @FindBy(xpath = "//*[text() = 'ACCOUNT']")
    private Label accountLabel;
    @FindBy(xpath = "//*[contains(text(),'Profile')]")
    private Button profileButton;
    @FindBy(xpath = "//*[contains(text(),'Achievements')]")
    private Button achievementsButton;
    @FindBy(xpath = "//*[contains(text(),'Requests')]")
    private Button requestsButton;

    @Step("verifying if Account Label is displayed")
    public Boolean isAccountLabelDisplayed() {
        LOGGER.info("verifying if Account Label is displayed");
        return accountLabel.isDisplayed();
    }

    @Step("verifying if Profile Button is present")
    public boolean isProfileButtonPresent() {
        LOGGER.info("verifying if Profile Button is present");
        return profileButton.isDisplayed();
    }

    @Step("clicking Profile Button")
    public UserAccountPage clickOnProfileButton() {
        LOGGER.info("clicking Profile Button");
        profileButton.click();
        return this;
    }

    @Step("verifying if Achievements Button is present")
    public boolean isAchievementsButtonPresent() {
        LOGGER.info("verifying if Achievements Button is present");
        return achievementsButton.isDisplayed();
    }

    @Step("clicking Achievements Button")
    public UserAccountPage clickOnAchievementsButton() {
        LOGGER.info("clicking Achievements Button");
        achievementsButton.click();
        return this;
    }

    @Step("verifying if Requests Button is present")
    public boolean isRequestsButtonPresent() {
        LOGGER.info("verifying if Requests Button is present");
        return requestsButton.isDisplayed();
    }

    @Step("clicking Requests Button")
    public UserAccountPage clickOnRequestsButton() {
        LOGGER.info("clicking Requests Button");
        requestsButton.click();
        return this;
    }

    @Step("verifying if is on user profile page")
    public boolean isOnUserAccountPage() {
        LOGGER.info("verifying if is on user profile page");
        return accountLabel.isDisplayed();
    }
}
