package com.epam.pageobjects.implementations;

import com.epam.models.TrainingRequestModel;
import com.epam.pageobjects.AbstractPageObject;
import com.epam.utils.CustomActions;
import com.epam.utils.DriverManager;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import com.epam.utils.enums.TrainingTime;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class AccountRequestsSectionPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(AccountRequestsSectionPage.class);
    @FindBy(xpath = "//*/child::p[contains(text(),'Sport Type')]/following-sibling::*")
    private List<Label> sportTypeLabels;
    @FindBy(xpath = "//*/child::p[contains(text(),'Gender')]/following-sibling::*")
    private List<Label> genderLabels;
    @FindBy(xpath = "//*/child::p[contains(text(),'Maturity level')]/following-sibling::*")
    private List<Label> maturityLevelLabels;
    @FindBy(xpath = "//*/child::p[contains(text(),'Time')]/following-sibling::*")
    private List<Label> timeLabels;
    @FindBy(xpath = "//button/child::span[text()='EDIT']")
    private List<Button> editTrainingRequestButton;
    @FindBy(xpath = "//button/child::span[text()='SAVE']")
    private Button saveEditedRequestButton;
    @FindBy(xpath = "//button/child::span[text()='DELETE']")
    private List<Button> deleteTrainingRequestButtons;
    @FindBy(xpath = "//button/child::span[text()=' CREATE A NEW REQUEST']")
    private Button createNewReqestButton;

    @Step("clicking create-New-Request Button")
    public AccountRequestsSectionPage clickCreateNewRequestButton() {
        LOGGER.info("clicking create-New-Request Button");
        CustomActions.scrollToElement(DriverManager.getDriver(),createNewReqestButton.getAsWebElement());
        createNewReqestButton.click();
        return this;
    }

    @Step("clicking first edit-Request Button")
    public AccountRequestsSectionPage clickFirstEditRequestButton() {
        LOGGER.info("clicking first edit-Request Button");
        CustomActions.scrollToElement(DriverManager.getDriver(),editTrainingRequestButton.get(0).getAsWebElement());
        editTrainingRequestButton.get(0).click();
        return this;
    }

    @Step("clicking save edited Request Button")
    public AccountRequestsSectionPage clickSaveEditedRequestButton() {
        LOGGER.info("clicking save edited Request Button");
        CustomActions.scrollToElement(DriverManager.getDriver(),saveEditedRequestButton.getAsWebElement());
        saveEditedRequestButton.click();
        return this;
    }

    @Step("clicking first delete-training-request Button")
    public AccountRequestsSectionPage clickDeleteFirstRequestButton() {
        LOGGER.info("clicking first delete-training-request Button");
        CustomActions.scrollToElement(DriverManager.getDriver(),deleteTrainingRequestButtons.get(0).getAsWebElement());
        deleteTrainingRequestButtons.get(0).click();
        return this;
    }

    @Step("getting Partners' Sport Type")
    public List<String> getPartnersSportTypeList() {
        LOGGER.info("getting Partners' Sport Type");
        List<String> sportTypesList = new ArrayList<>();
        for (Label sportType : sportTypeLabels) {
            sportTypesList.add(sportType.getText());
        }
        return sportTypesList;
    }

    @Step("getting Partners' gender")
    public List<String> getPartnersGenderList() {
        LOGGER.info("getting Partners' gender");
        List<String> genderList = new ArrayList<>();
        for (Label gender : genderLabels) {
            genderList.add(gender.getText());
        }
        return genderList;
    }

    @Step("getting Partners' Maturity Level")
    public List<String> getPartnersMaturityLevelList() {
        LOGGER.info("getting Partners' Maturity Level");
        List<String> maturityLevelList = new ArrayList<>();
        for (Label maturityLevel : maturityLevelLabels) {
            maturityLevelList.add(maturityLevel.getText());
        }
        return maturityLevelList;
    }

    @Step("getting Partners' Time")
    public List<String> getPartnersTimeList() {
        LOGGER.info("getting Partners' Time");
        List<String> timeList = new ArrayList<>();
        for (Label time : timeLabels) {
            timeList.add(time.getText());
        }
        return timeList;
    }

    @Step("getting First Training Request Model")
    public TrainingRequestModel getFirstTrainingRequestModel() {
        LOGGER.info("getting First Training Request Model:");
        TrainingTime trainingTime = getPartnersTimeList().get(0).toUpperCase()
                .equals(TrainingTime.ALL_DAY.getTrainingTime()) ?
                TrainingTime.ALL_DAY : getTime(getPartnersTimeList().get(0));
        TrainingRequestModel trainingRequestModel = TrainingRequestModel.builder()
                .sportType(SportType.valueOf(getPartnersSportTypeList().get(0).toUpperCase()))
                .gender(Gender.valueOf(getPartnersGenderList().get(0).toUpperCase()))
                .maturityLevel(MaturityLevel.valueOf(getPartnersMaturityLevelList().get(0).toUpperCase()))
                .trainingTime(trainingTime).build();
        LOGGER.info(trainingRequestModel.toString());
        return trainingRequestModel;
    }

    private TrainingTime getTime(String time) {
        return TrainingTime.valueOf(time.toUpperCase().substring(0, time.length() - 1));
    }

    @Step("verifying are there any requests")
    public boolean areAnyRequestsPresent() {
        LOGGER.info("verifying are there any requests");
        return !sportTypeLabels.isEmpty();
    }
}
