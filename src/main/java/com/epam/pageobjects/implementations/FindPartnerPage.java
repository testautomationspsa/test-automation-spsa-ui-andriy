package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.CheckBox;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class FindPartnerPage extends AbstractPageObject {
    private static final String FIND_PARTNERS_PAGE_URI = "https://SportBuddies.com/findPartners";
    private static final Logger LOGGER = LogManager.getLogger(FindPartnerPage.class);
    @FindBy(xpath = "//mat-select[@formcontrolname='sportType']")
    private Button sportTypeButton;
    @FindBy(xpath = "//span[contains(text(),'Running')]/parent::mat-option")
    private Button sportTypeRunning;
    @FindBy(xpath = "//span[contains(text(),'Swimming')]/parent::mat-option")
    private Button sportTypeSwimming;
    @FindBy(xpath = "//span[contains(text(),'Football')]/parent::mat-option")
    private Button sportTypeFootball;
    @FindBy(xpath = "//span[contains(text(),'Yoga')]/parent::mat-option")
    private Button sportTypeYoga;
    @FindBy(xpath = "//*[@id='morning']")
    private CheckBox morningCheckBox;
    @FindBy(xpath = "//*[@id='afternoon']")
    private CheckBox afternoonCheckBox;
    @FindBy(xpath = "//*[@id='evening']")
    private CheckBox eveningCheckBox;
    @FindBy(xpath = "//*[@id='all']")
    private CheckBox allDayCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Male')]")
    private CheckBox maleCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Female')]")
    private CheckBox femaleCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Both')]")
    private CheckBox bothGendersCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Beginner')]")
    private CheckBox beginnerCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Middle')]")
    private CheckBox middleCheckBox;
    @FindBy(xpath = "//*[@class = 'mat-radio-label-content' and contains(text(),'Professional')]")
    private CheckBox professionalCheckBox;
    @FindBy(xpath = "//*[contains(text(),'SUBMIT')]/parent::button")
    private Button submitButton;
    @FindBy(xpath = "//*[@class='partner-card__info']/child::p[contains(text(),'Sport Type')]/following-sibling::*")
    private List<Label> sportTypeLabels;
    @FindBy(xpath = "//*[@class='partner-card__info']/child::p[contains(text(),'Gender')]/following-sibling::*")
    private List<Label> genderLabels;
    @FindBy(xpath = "//*[@class='partner-card__info']/child::p[contains(text(),'Maturity level')]/following-sibling::*")
    private List<Label> maturityLevelLabels;
    @FindBy(xpath = "//*[@class='partner-card__info']/child::p[contains(text(),'Time')]/following-sibling::*")
    private List<Label> timeLabels;
    @FindBy(linkText = "Send Email")
    private List<Button> emailToOtherUserButtons;

    @Step("going to Find Partners Page")
    public FindPartnerPage goToFindPartnersPage() {
        LOGGER.info("going to Find Partners Page");
        driver.get(FIND_PARTNERS_PAGE_URI);
        return this;
    }

    @Step("clicking sportType button")
    public FindPartnerPage clickSportTypeButton() {
        LOGGER.info("clicking sportType button");
        sportTypeButton.click();;
        return this;
    }

    @Step("clicking sportType RUNNING  button")
    public FindPartnerPage clickSportTypeRunningButton() {
        LOGGER.info("clicking sportType RUNNING button");
        sportTypeRunning.click();
        return this;
    }

    @Step("clicking sportType SWIMMING  button")
    public FindPartnerPage clickSportTypeSwimmingButton() {
        LOGGER.info("clicking sportType SWIMMING button");
        sportTypeSwimming.click();
        return this;
    }

    @Step("clicking sportType FOOTBALL  button")
    public FindPartnerPage clickSportTypeFootballButton() {
        LOGGER.info("clicking sportType FOOTBALL button");
        sportTypeFootball.click();
        return this;
    }

    @Step("clicking sportType YOGA  button")
    public FindPartnerPage clickSportTypeYogaButton() {
        LOGGER.info("clicking sportType YOGA button");
        sportTypeYoga.click();
        return this;
    }

    @Step("checking Out Morning CheckBox.")
    public FindPartnerPage checkingOutMorningCheckBox() {
        LOGGER.info("checking Out Morning CheckBox.");
        morningCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Afternoon CheckBox.")
    public FindPartnerPage checkingOutAfternoonCheckBox() {
        LOGGER.info("checking Out Afternoon CheckBox.");
        afternoonCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Evening CheckBox.")
    public FindPartnerPage checkingOutEveningCheckBox() {
        LOGGER.info("checking Out Evening CheckBox.");
        eveningCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out All Day CheckBox.")
    public FindPartnerPage checkingOutAllDayCheckBox() {
        LOGGER.info("checking Out All Day CheckBox.");
        allDayCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Male CheckBox.")
    public FindPartnerPage checkingOutMaleCheckBox() {
        LOGGER.info("checking Out Male CheckBox.");
        maleCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Female CheckBox.")
    public FindPartnerPage checkingOutFemaleCheckBox() {
        LOGGER.info("checking Out Female CheckBox.");
        femaleCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Both Genders CheckBox.")
    public FindPartnerPage checkingOutBothGendersCheckBox() {
        LOGGER.info("checking Out Both Genders CheckBox.");
        bothGendersCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Beginner CheckBox.")
    public FindPartnerPage checkingOutBeginnerCheckBox() {
        LOGGER.info("checking Out Beginner CheckBox.");
        beginnerCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Middle CheckBox.")
    public FindPartnerPage checkingOutMiddleCheckBox() {
        LOGGER.info("checking Out Middle CheckBox.");
        middleCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("checking Out Professional CheckBox.")
    public FindPartnerPage checkingOutProfessionalCheckBox() {
        LOGGER.info("checking Out Professional CheckBox.");
        professionalCheckBox.setCheckBoxSelected();
        return this;
    }

    @Step("clicking Submit Button.")
    public FindPartnerPage clickSubmitButton() {
        LOGGER.info("clicking Submit Button.");
        submitButton.click();
        return this;
    }

    @Step("getting Partners' Sport Type")
    public List<String> getPartnersSportTypeList() {
        LOGGER.info("getting Partners' Sport Type");
        List<String> sportTypesList = new ArrayList<>();
        for (Label sportType : sportTypeLabels) {
            sportTypesList.add(sportType.getText());
        }
        return sportTypesList;
    }

    @Step("getting Partners' gender")
    public List<String> getPartnersGenderList() {
        LOGGER.info("getting Partners' gender");
        List<String> genderList = new ArrayList<>();
        for (Label gender : genderLabels) {
            genderList.add(gender.getText());
        }
        return genderList;
    }

    @Step("getting Partners' Maturity Level")
    public List<String> getPartnersMaturityLevelList() {
        LOGGER.info("getting Partners' Maturity Level");
        List<String> maturityLevelList = new ArrayList<>();
        for (Label maturityLevel : maturityLevelLabels) {
            maturityLevelList.add(maturityLevel.getText());
        }
        return maturityLevelList;
    }

    @Step("getting Partners' Time")
    public List<String> getPartnersTimeList() {
        LOGGER.info("getting Partners' Time");
        List<String> timeList = new ArrayList<>();
        for (Label time : timeLabels) {
            timeList.add(time.getText());
        }
        return timeList;
    }

    @Step("isOnFindPartnersPage check")
    public boolean isOnFindPartnersPage() {
        return driver.getCurrentUrl().contains(FIND_PARTNERS_PAGE_URI);
    }

    @Step("are Email-To-Other-User-Buttons Enabled")
    public boolean isEmailToOtherUserButtonsEnabled() {
        LOGGER.info("There are " + emailToOtherUserButtons.size() + " partners available.");
        for (Button button : emailToOtherUserButtons) {
            if (!button.isEnabled()) {
                return false;
            }
        }
        return true;
    }

    @Step("unchecking everything")
    public void uncheckAll() {
        LOGGER.info("unchecking everything");
        morningCheckBox.setCheckBoxUnSelected();
        afternoonCheckBox.setCheckBoxUnSelected();
        eveningCheckBox.setCheckBoxUnSelected();
        allDayCheckBox.setCheckBoxUnSelected();
        maleCheckBox.setCheckBoxUnSelected();
        femaleCheckBox.setCheckBoxUnSelected();
        bothGendersCheckBox.setCheckBoxUnSelected();
        beginnerCheckBox.setCheckBoxUnSelected();
        middleCheckBox.setCheckBoxUnSelected();
        professionalCheckBox.setCheckBoxUnSelected();
    }
}
