package com.epam.pageobjects.implementations;

import com.epam.utils.CustomActions;
import com.epam.utils.DriverManager;
import com.epam.utils.Waiter;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.InputTextField;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class BlogsPage extends SportBuddiesHomePage {
    private static final String SPORT_BUDDIES_BLOG_PAGE_URI = "https://sport-frontend.firebaseapp.com/blog";
    private static final Logger LOGGER = LogManager.getLogger(BlogsPage.class);
    @FindBy(xpath = "//h1[contains(@class, 'blog__header') and text() = 'BLOG']")
    private Label blogsIdentifier;
    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and text() = 'Blog']")
    private Label sportBuddiesLogo;
    @FindBy(linkText = "READ MORE")
    private List<Button> blogSeeMoreButtons;
    @FindBy(id = "searchByTagField")  // NONE
    private InputTextField searchByTagField;
    @FindBy(id = "searchByTagButon")  // NONE
    private Button searchByTagButon;
    @FindBy(xpath = "//button[@class='mat-paginator-navigation-previous mat-icon-button mat-button-base']")
    private Button paginationFirstButton;

    @Step("inputting '{0}' into Search By Tag Field")
    public BlogsPage writeInSearchByTagField(String tag) {
        LOGGER.info("inputting '" + tag + "' into Search By Tag Field");
        searchByTagField.sendKeys(tag);
        return this;
    }

    @Step("clicking Search By Tag Button")
    public BlogsPage clickSearchByTagButton() {
        LOGGER.info("clicking Search By Tag Button");
        searchByTagButon.click();
        return this;
    }

    @Step("verifying Blogs Identifier")
    public boolean isBlogPresent() {
        LOGGER.info("verifying Blogs Identifier");
        return blogsIdentifier.isDisplayed();
    }

    @Step("verifying Logo Identifier")
    public boolean isLogoPresent() {
        LOGGER.info("verifying Logo Identifier");
        return sportBuddiesLogo.isDisplayed();
    }

    @Step("isOnBlogsPage check")
    public boolean isOnBlogPage() {
        return driver.getCurrentUrl().contains(SPORT_BUDDIES_BLOG_PAGE_URI);
    }

    @Step("clicking On First Blog Details Button")
    public BlogDetailsPage clickOnFirstBlogDetailsButton() {
        LOGGER.info("clicking On First Blog Details Button");
        CustomActions.clickOnBody(DriverManager.getDriver());
        CustomActions.scrollToTop(DriverManager.getDriver());
        Waiter.waitForElementToBeClickable(blogSeeMoreButtons.get(0), Waiter.SMALL_WAIT);
        Waiter.waitFor(8);
        blogSeeMoreButtons.get(0).click();
        return new BlogDetailsPage();
    }

    @Step("verifying Pagination Identifier")
    public boolean isPaginationPresent() {
        LOGGER.info("verifying Pagination Identifier");
        CustomActions.scrollToTop(DriverManager.getDriver());
        return paginationFirstButton.isDisplayed();
    }
}
