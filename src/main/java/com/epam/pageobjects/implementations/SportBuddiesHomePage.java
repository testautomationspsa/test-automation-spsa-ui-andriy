package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.utils.Constants;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;


public class SportBuddiesHomePage extends AbstractPageObject {
    private static final String SPORT_BUDDIES_MAIN_PAGE_URI = Constants.SPORT_BUDDIES_URI;
    private static final Logger LOGGER = LogManager.getLogger(SportBuddiesHomePage.class);
    @FindBy(linkText = "Sign In")
    private Button signInButton;
    @FindBy(linkText = "Sign Up")
    private Button signUpButton;
    @FindBy(xpath = "//div[contains(@class,'header__logo') and not(contains(@class,'header__logo-and-blog'))]")
    private Label sportBuddiesLogo;
    @FindBy(xpath = "//*[contains(@class,'header__profile-button mat-menu-trigger mat-icon-button mat-button-base ng-star-inserted')]")
    private Button accountIconButton;
    @FindBy(linkText = "My Account")
    private Button myAccountButton;
    @FindBy(linkText = "Sign Out")
    private Button signOutButton;
    @FindBy(xpath = "//*[contains(text(),'Search')]")
    private Button searchDropDown;
    @FindBy(linkText = "Search a Partner")
    private Button searchPartner;
    @FindBy(linkText = "Search a Location")
    private Button searchLocation;
    @FindBy(linkText = "Search a Trainer")
    private Button searchTrainer;
    @FindBy(xpath = "//a[@href='/blog']")
    private Button blogButton;

    @Step("going to User Account Page")
    public UserAccountPage goToUserAccountPage() {
        LOGGER.info("going to User Account Page");
        accountIconButton.click();
        myAccountButton.click();
        return new UserAccountPage();
    }

    @Step("going to Sport Buddies Main Page")
    public SportBuddiesHomePage goToSportBuddiesMainPage() {
        LOGGER.info("going to Sport Buddies Main Page");
        driver.get(SPORT_BUDDIES_MAIN_PAGE_URI);
        return this;
    }

    @Step("clicking Sign In Button")
    public SportBuddiesHomePage clickSignInButton() {
        LOGGER.info("clicking Sign In Button");
        signInButton.click();
        return this;
    }

    @Step("clicking Blog Button")
    public SportBuddiesHomePage clickBlogButton() {
        LOGGER.info("clicking Blog Button");
        blogButton.click();
        return this;
    }

    @Step("clicking Sign Up Button")
    public SportBuddiesHomePage clickSignUpButton() {
        LOGGER.info("clicking Sign Up Button");
        signUpButton.click();
        return this;
    }

    @Step("clicking Search Button")
    public SportBuddiesHomePage clickSearchButton() {
        LOGGER.info("clicking Search Button");
        searchDropDown.click();
        return this;
    }

    @Step("clicking Search Location Button")
    public SportBuddiesHomePage clickSearchLocationButton() {
        LOGGER.info("clicking Search Location Button");
        searchLocation.click();
        return this;
    }

    @Step("clicking Search Partner Button")
    public SportBuddiesHomePage clickSearchPartnerButton() {
        LOGGER.info("clicking Search Partner Button");
        searchPartner.click();
        return this;
    }

    @Step("clicking Search Trainer Button")
    public SportBuddiesHomePage clickSearchTrainerButton() {
        LOGGER.info("clicking Search Trainer Button");
        searchTrainer.click();
        return this;
    }

    @Step("verifying Logo Identifier")
    public boolean isLogoPresent() {
        LOGGER.info("verifying Logo Identifier");
        return sportBuddiesLogo.isDisplayed();
    }

    @Step("verifying Sign In Identifier")
    public boolean isSignInButtonPresent() {
        LOGGER.info("verifying Sign In Identifier");
        return signInButton.isDisplayed();
    }

    @Step("verifying Sign Up Identifier")
    public boolean isSignUpButtonPresent() {
        LOGGER.info("verifying Sign Up Identifier");
        return signUpButton.isDisplayed();
    }

    @Step("verifying Search Dropdown Identifier")
    public boolean isSearchDopDownPresent() {
        LOGGER.info("verifying Search Dropdown Identifier");
        return searchDropDown.isDisplayed();
    }

    @Step("verifying is Blog Button Present")
    public boolean isBlogButtonPresent() {
        LOGGER.info("verifying is Blog Button Present");
        return blogButton.isDisplayed();
    }

    @Step("verifying if is on SportBuddies main page")
    public boolean isOnSportBuddiesMainPage() {
        return driver.getCurrentUrl().contains(SPORT_BUDDIES_MAIN_PAGE_URI);
    }

    @Step("clicking Sign Out Button")
    public void clickSignOutButton() {
        LOGGER.info("clicking Sign Out Button");
        signOutButton.click();
    }

    @Step("clicking on Profile Icon")
    public SportBuddiesHomePage clickOnProfileIcon() {
        LOGGER.info("clicking on Profile Icon");
        accountIconButton.click();
        return this;
    }

    @Step("clicking My Account Button")
    public UserAccountPage clickOnMyAccountButton() {
        LOGGER.info("clicking My Account Button");
        myAccountButton.click();
        return new UserAccountPage();
    }

    @Step("verifying if Profile Icon is displayed")
    public boolean isProfileIconButtonDisplayed() {
        LOGGER.info("verifying if Profile Icon is displayed");
        return accountIconButton.isDisplayed();
    }
}
