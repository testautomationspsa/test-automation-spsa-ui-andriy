package com.epam.pageobjects.implementations;

import com.epam.pageobjects.AbstractPageObject;
import com.epam.utils.CustomActions;
import com.epam.decorator.webelement.implementations.Button;
import com.epam.decorator.webelement.implementations.InputTextField;
import com.epam.decorator.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class FindVenuePage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(FindVenuePage.class);
    @FindBy(xpath = "//mat-select[@formcontrolname='sportType']")
    private Button sportTypeButton;
    @FindBy(xpath = "//span[contains(text(),'Running')]/parent::mat-option")
    private Button sportTypeRunning;
    @FindBy(xpath = "//span[contains(text(),'Swimming')]/parent::mat-option")
    private Button sportTypeSwimming;
    @FindBy(xpath = "//span[contains(text(),'Football')]/parent::mat-option")
    private Button sportTypeFootball;
    @FindBy(xpath = "//span[contains(text(),'Yoga')]/parent::mat-option")
    private Button sportTypeYoga;
    @FindBy(xpath = "//mat-select[@formcontrolname='locationType']")
    private Button locationButton;
    @FindBy(xpath = "//span[contains(text(),'Near my home')]/parent::mat-option")
    private Button locationNearHome;
    @FindBy(xpath = "//span[contains(text(),'Other')]/parent::mat-option")
    private Button locationOther;
    @FindBy(xpath = "//mat-select[@formcontrolname='inOut']")
    private Button venueTypeButton;
    @FindBy(xpath = "//span[contains(text(),'Indoor')]/parent::mat-option")
    private Button venueTypeIndoor;
    @FindBy(xpath = "//span[contains(text(),'Outdoor')]/parent::mat-option")
    private Button venueTypeOutdoor;
    @FindBy(xpath = "//div[text()='Спорт мастер 3']")
    private Button testVenueOnMap;
    @FindBy(xpath = "(//h3[text()='Спорт мастер 3']/parent::div)/child::button/child::span")
    private Button testVenueMoreInfo;
    @FindBy(xpath = "//input")
    private InputTextField locationInputField;
    @FindBy(xpath = "//button[@class='search-venue__submit mat-raised-button mat-button-base mat-primary']")
    private Button searchButton;
    @FindBy(xpath = "//div[@class='pac-item']")
    private List<Button> googleMapsSuggestions;
    @FindBy(xpath = "//div[text()='Venue details']")
    private Label venueDetailsLabel;

    @Step("clicking on Test Venue")
    public void clickTestVenueOnMap() {
        LOGGER.info("clicking on Test Venue");
        Actions builder = new Actions(driver);
        builder.moveToElement(testVenueOnMap.getAsWebElement(), 0, 0).click().build().perform();
    }

    @Step("verifying is Venue Details Label Displayed")
    public boolean isVenueDetailsLabelDisplayed() {
        LOGGER.info("verifying is Venue Details Label Displayed");
        return venueDetailsLabel.isDisplayed();
    }

    @Step("verifying is Location Input Field Enabled")
    public boolean isLocationInputFieldEnabled() {
        LOGGER.info("verifying is Location Input Field Enabled");
        return locationInputField.isEnabled();
    }

    @Step("clicking on Location input field")
    public void clickLocationInputField() {
        LOGGER.info("clicking on Location input field");
        locationInputField.click();
    }

    @Step("writing into Location input field '{0}'")
    public void writeLocation(String location) {
        LOGGER.info("writing into Location input field'" + location + "'");
        locationInputField.sendKeys(location);
    }

    @Step("selecting First Suggestion")
    public void selectFirstSuggestion() {
        LOGGER.info("selecting First Suggestion");
        googleMapsSuggestions.get(0).click();
    }

    @Step("clicking on Test Venue 'More info' button")
    public void clickTestVenueMoreInfoButton() {
        LOGGER.info("clicking on Test Venue 'More info' button");
        testVenueMoreInfo.click();
    }

    @Step("clicking Search Button")
    public void clickSearchButton() {
        LOGGER.info("clicking Search Button");
        searchButton.click();
    }

    @Step("verifying is Search Button Present")
    public boolean isSearchButtonPresent() {
        LOGGER.info("verifying is Search Button Present");
        return searchButton.isDisplayed();
    }

    @Step("verifying is MORE INFO button Displayed")
    public boolean isMoreInfoButtonDisplayed() {
        LOGGER.info("verifying is MORE INFO button Displayed");
        return testVenueMoreInfo.isDisplayed();
    }

    @Step("clicking Sport Type Button")
    public void clickSportTypeButton() {
        LOGGER.info("clicking Sport Type Button");
        sportTypeButton.click();
    }

    @Step("clicking Sport Type RUNNING Button")
    public void clickSportTypeRUNNINGButton() {
        LOGGER.info("clicking Sport Type RUNNING Button");
        sportTypeRunning.click();
    }

    @Step("verifying is Running Option Present")
    public boolean isRunningEnabled() {
        LOGGER.info("verifying is Running Option Present");
        return sportTypeRunning.isEnabled();
    }

    @Step("verifying is Swimming Option Present")
    public boolean isSwimmingEnabled() {
        LOGGER.info("verifying is Swimming Option Present");
        return sportTypeSwimming.isEnabled();
    }

    @Step("verifying is Football Option Present")
    public boolean isFootballEnabled() {
        LOGGER.info("verifying is Football Option Present");
        return sportTypeFootball.isEnabled();
    }

    @Step("verifying is Yoga Option Present")
    public boolean isYogaEnabled() {
        LOGGER.info("verifying is Yoga Option Present");
        return sportTypeYoga.isEnabled();
    }

    @Step("clicking Location Button")
    public void clickLocationButton() {
        LOGGER.info("clicking Location Button");
        CustomActions.clickOnBody(driver);
        locationButton.click();
    }

    @Step("verifying is Near Home Option Present")
    public boolean isNearMyHomeEnabled() {
        LOGGER.info("verifying is Near Home Option Present");
        return locationNearHome.isEnabled();
    }

    @Step("clicking Other Option")
    public void clickOtherOption() {
        LOGGER.info("clicking Other Option");
        locationOther.click();
    }

    @Step("verifying is Other Option Present")
    public boolean isOtherEnabled() {
        LOGGER.info("verifying is Other Option Present");
        return locationOther.isEnabled();
    }

    @Step("clicking Venue Type Button")
    public void clickVenueTypeButton() {
        LOGGER.info("clicking Venue Type Button");
        venueTypeButton.click();
    }

    @Step("clicking Indoor Option")
    public void clickIndoorOption() {
        LOGGER.info("clicking Indoor Option");
        venueTypeIndoor.click();
    }

    @Step("verifying is Indoor Option Present")
    public boolean isIndoorEnabled() {
        LOGGER.info("verifying is Indoor Option Present");
        return venueTypeIndoor.isEnabled();
    }

    @Step("verifying is Outdoor Option Present")
    public boolean isOutdoorEnabled() {
        LOGGER.info("verifying is Outdoor Option Present");
        return venueTypeOutdoor.isEnabled();
    }
}
